﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class ProductoPedido
    {
        CadenaConexion stringConexion = new CadenaConexion();
        SqlConnection objConnection;
        public int EditarStock(Entidades.ProductoPedido productoAEditar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.EDITARSTOCKPORVENTAS", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Ing1", productoAEditar.Ing1);
                cmd.Parameters.AddWithValue("@Cant1", productoAEditar.Cant1);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }
    }
}
