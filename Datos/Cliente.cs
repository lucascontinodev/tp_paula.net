﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
	public class Cliente
	{

        CadenaConexion stringConexion = new CadenaConexion();
        SqlConnection objConnection;


        public string InsertCliente(Entidades.Cliente clienteAInsertar)
        {
            string respuesta = "";
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.INSERTCLIENTE", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Nombre", clienteAInsertar.Nombre);
                cmd.Parameters.AddWithValue("@Apellido", clienteAInsertar.Apellido);
                cmd.Parameters.AddWithValue("@Dni", clienteAInsertar.Dni);
                cmd.Parameters.AddWithValue("@Domicilio", clienteAInsertar.Domicilio);
                cmd.Parameters.AddWithValue("@Telefono", clienteAInsertar.Telefono);
                cmd.ExecuteReader();
                respuesta = "OK";
            }
            catch (Exception)
            {
                respuesta = "Error al insertar";
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public DataSet ObtenerClientes()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select * from Cliente";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "clientes");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public string ObtenerClientePorDni(Entidades.Cliente cliente)
        {
            string dni = "";
            var con = new SqlConnection(stringConexion.cadena);

            try
            {

                using (con)
                {
                    var sql = "SELECT dni FROM Cliente where Dni = @dni";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@dni", cliente.Dni);
                        con.Open();
                        dni = (string)cmd.ExecuteScalar();
                    }

                }

            }
            catch (Exception)
            {
                dni = "";
            }
            finally
            {
                con.Close();
            }
            return dni;
        }

        public int EditarCliente(Entidades.Cliente clienterAEditar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.EDITARCLIENTE", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Cliente", clienterAEditar.IdCliente);
                cmd.Parameters.AddWithValue("@Telefono", clienterAEditar.Telefono);
                cmd.Parameters.AddWithValue("@Domicilio", clienterAEditar.Domicilio);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }
    }
}
