﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Proveedor
    {

        CadenaConexion stringConexion = new CadenaConexion();
        SqlConnection objConnection;

        public string InsertProveedor(Entidades.Proveedor proveedorAInsertar)
        {
            string respuesta = "";
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.INSERTPROVEEDOR", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CUIT", proveedorAInsertar.Cuit);
                cmd.Parameters.AddWithValue("@Domicilio", proveedorAInsertar.Domicilio);
                cmd.Parameters.AddWithValue("@Email", proveedorAInsertar.Email);
                cmd.Parameters.AddWithValue("@RazonSocial", proveedorAInsertar.RazonSocial);
                cmd.Parameters.AddWithValue("@Telefono", proveedorAInsertar.Telefono);
                cmd.ExecuteReader();
                respuesta = "OK";
            }
            catch (Exception)
            {
                respuesta = "Error al insertar";
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public int EliminarProveedor(Entidades.Proveedor proveedorAEliminar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.ELIMINARPROVEEDOR", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", proveedorAEliminar.Id_proveedor);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public int EditarProveedor(Entidades.Proveedor proveedorAEditar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.EDITARPROVEEDOR", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", proveedorAEditar.Id_proveedor);
                cmd.Parameters.AddWithValue("@Telefono", proveedorAEditar.Telefono);
                cmd.Parameters.AddWithValue("@email", proveedorAEditar.Email);
                cmd.Parameters.AddWithValue("@Domicilio", proveedorAEditar.Domicilio);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public DataSet ObtenerProveedores()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select * from Proveedores where Estado = 'Activo'";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "proveedores");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }
    }
}
