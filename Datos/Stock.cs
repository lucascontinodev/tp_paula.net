﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Stock
    {
        CadenaConexion stringConexion = new CadenaConexion();
        SqlConnection objConnection;
        public DataSet ObtenerStock()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select s.Id_Stock as 'Cod Stock', s.descripcion, s.Cantidad, s.Cantidad_Minima, s.Precio, " +
                "(u.Nombre + ' ' + u.Apellido) as 'Encargado', p.RazonSocial as 'Proveedor' from Stock s " +
                "inner join Proveedores p on s.Id_Proveedor = p.Id_proveedor inner join Usuario u on u.Id = s.id_usuario" +
                " where s.Estado = 'Activo'";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "productos");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerPuntoCritico()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select * from stock where Cantidad_Minima > = Cantidad and Estado = 'Activo'";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "productos");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public string InsertStock(Entidades.Stock productorAInsertar)
        {
            string respuesta = "";
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.INSERTSTOCK", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Descripcion", productorAInsertar.Descripcion);
                cmd.Parameters.AddWithValue("@Cantidad", productorAInsertar.Cantidad);
                cmd.Parameters.AddWithValue("@Cantidad_Minima", productorAInsertar.CantidadMinima);
                cmd.Parameters.AddWithValue("@Id_Proveedor", productorAInsertar.IdProveedor);
                cmd.Parameters.AddWithValue("@Id_Usuario", productorAInsertar.IdUsuario);
                cmd.Parameters.AddWithValue("@Precio", productorAInsertar.Precio);
                cmd.ExecuteReader();
                respuesta = "OK";
            }
            catch (Exception)
            {
                respuesta = "Error al insertar";
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public int EditarStock(Entidades.Stock productoAEditar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.EDITARSTOCK", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Stock", productoAEditar.IdStock);
                cmd.Parameters.AddWithValue("@Cantidad", productoAEditar.Cantidad);
                cmd.Parameters.AddWithValue("@Cantidad_Minima", productoAEditar.CantidadMinima);
                cmd.Parameters.AddWithValue("@Precio", productoAEditar.Precio);
                cmd.Parameters.AddWithValue("@Id_Usuario", productoAEditar.IdUsuario);
                cmd.Parameters.AddWithValue("@Id_Proveedor", productoAEditar.IdProveedor);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public int EliminarStock(Entidades.Stock productoAEliminar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.ELIMINARSTOCK", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Stock", productoAEliminar.IdStock);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public string ObtenerPrecioStock(int id_stock)
        {
            string precio = "";
            var con = new SqlConnection(stringConexion.cadena);

            try
            {

                using (con)
                {
                    var sql = "select precio from Stock where Id_Stock = @Id_Stock";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@Id_Stock", id_stock);
                        con.Open();
                        precio = cmd.ExecuteScalar().ToString();
                    }

                }

            }
            catch (Exception)
            {
                precio = "";
            }
            finally
            {
                con.Close();
            }
            return precio;
        }

        public int ObtenerIdPorductoPorDescripcion(string descripcionProducto)
        {
            int id = 0;
            var con = new SqlConnection(stringConexion.cadena);

            try
            {

                using (con)
                {
                    var sql = "select Id_Stock from Stock where descripcion = @descripcion and Estado = 'Activo'";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@descripcion", descripcionProducto);
                        con.Open();
                        id = (int)cmd.ExecuteScalar();
                    }

                }

            }
            catch (Exception)
            {
                id = 0;
            }
            finally
            {
                con.Close();
            }
            return id;
        }
    }
}
