﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class Venta
    {
        CadenaConexion stringConexion = new CadenaConexion();
        SqlConnection objConnection;
        public int ObtenerIdUltimaVenta()
        {
            int id = 0;
            var con = new SqlConnection(stringConexion.cadena);

            try
            {

                using (con)
                {
                    var sql = "select MAX(Id_Venta) from Ventas";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        con.Open();
                        id = (int)cmd.ExecuteScalar();
                    }

                }

            }
            catch (Exception)
            {
                id = 0;
            }
            finally
            {
                con.Close();
            }
            return id;
        }

        public string InsertVenta(Entidades.Venta ventaAInsertar)
        {
            string respuesta = "";
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.INSERTVENTA", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Venta", ventaAInsertar.IdVenta);
                cmd.Parameters.AddWithValue("@Id_Usuario", ventaAInsertar.IdUsuario);
                cmd.Parameters.AddWithValue("@Id_Producto", ventaAInsertar.IdProdcuto);
                cmd.Parameters.AddWithValue("@Cantidad", ventaAInsertar.Cantidad);
                cmd.Parameters.AddWithValue("@Fecha", ventaAInsertar.Fecha);
                cmd.Parameters.AddWithValue("@Medio_Pago", ventaAInsertar.MedioPago);
                cmd.Parameters.AddWithValue("@Monto_Total", ventaAInsertar.MontoTotal);
                cmd.ExecuteReader();
                respuesta = "OK";
            }
            catch (Exception)
            {
                respuesta = "Error al insertar";
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public DataSet ObtenerVentas()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select distinct Id_Venta from ventas";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ventas");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerMedioPago()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select distinct Medio_Pago from Ventas";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ventas");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerUsuario()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select distinct (u.Nombre + ' ' + u.Apellido) as 'Usuario' from Ventas v inner join Usuario u on v.id_usuario = u.Id";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ventas");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerProducto()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select distinct s.descripcion from ventas v inner join Stock s on s.Id_Stock = v.Id_producto";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ventas");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerListaPorId(int IdVenta)
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select s.descripcion, v.cantidad, v.MontoTotal, v.Medio_Pago, v.Fecha, " +
                "(u.nombre + ' ' + u.apellido) as 'Usuario' from Ventas v " +
                "inner join Stock s on v.Id_producto = s.Id_Stock " +
                "inner join Usuario u on v.id_usuario = u.Id where v.Id_Venta = @IdVenta";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@IdVenta", IdVenta);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ListaPorId");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerListaPorMedioPago(string MedioPago)
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select v.Id_Venta, s.descripcion, v.cantidad, v.MontoTotal, v.Medio_Pago, v.Fecha, " +
                "(u.nombre + ' ' + u.apellido) as 'Usuario' from Ventas v " +
                "inner join Stock s on v.Id_producto = s.Id_Stock " +
                "inner join Usuario u on v.id_usuario = u.Id where v.Medio_Pago = @MedioPago order by v.Id_Venta desc";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@MedioPago", MedioPago);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ListaPorMedioPago");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerListaPorUsuario(string Usuario)
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select v.Id_Venta, s.descripcion, v.cantidad, v.MontoTotal, v.Medio_Pago, v.Fecha, " +
                "(u.nombre + ' ' + u.apellido) as 'Usuario' from Ventas v " +
                "inner join Stock s on v.Id_producto = s.Id_Stock " +
                "inner join Usuario u on v.id_usuario = u.Id where (u.Nombre + ' ' + u.Apellido) = @Usuario order by v.Id_Venta desc";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@Usuario", Usuario);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ListaPorUsuario");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public DataSet ObtenerListaPorProducto(string Producto)
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select v.Id_Venta, s.descripcion, v.cantidad, v.MontoTotal, v.Medio_Pago, v.Fecha, " +
                "(u.nombre + ' ' + u.apellido) as 'Usuario' from Ventas v " +
                "inner join Stock s on v.Id_producto = s.Id_Stock " +
                "inner join Usuario u on v.id_usuario = u.Id where s.descripcion = @Producto order by v.Id_Venta desc";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@Producto", Producto);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "ListaPorProducto");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }
    }
}
