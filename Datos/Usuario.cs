﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
	public class Usuario
	{
        CadenaConexion stringConexion = new CadenaConexion();
        SqlConnection objConnection;

		public string InsertUsuario(Entidades.Login usuarioAInsertar,int rolObtenido)
		{
            string respuesta = "";
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.INSERTUSUARIO", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Rol", rolObtenido);
                cmd.Parameters.AddWithValue("@Nombre", usuarioAInsertar.Nombre);
                cmd.Parameters.AddWithValue("@Apellido", usuarioAInsertar.Apellido);
                cmd.Parameters.AddWithValue("@Dni", usuarioAInsertar.Dni);
                cmd.Parameters.AddWithValue("@Domicilio", usuarioAInsertar.Domicilio);
                cmd.Parameters.AddWithValue("@Edad", usuarioAInsertar.Edad);
                cmd.Parameters.AddWithValue("@Cuenta", usuarioAInsertar.Cuenta);
                cmd.Parameters.AddWithValue("@Contrasenia", usuarioAInsertar.Contrasenia);
                cmd.ExecuteReader();
                respuesta = "OK";
            }catch(Exception)
            {
                respuesta = "Error al insertar";
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
		}

        public string RolAsociado(int id_usuario)
        {
            string rol = "";
            var con = new SqlConnection(stringConexion.cadena);

            try
            {

                using (con)
                {
                    var sql = "SELECT r.Responsabilidad FROM Usuario u inner join Login l on u.Id = l.Id_Usuario" +
                        " inner join rol r on r.Id = u.Id_Rol where u.Id = @id_usuario";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@id_usuario", id_usuario);
                        con.Open();
                        rol = (string)cmd.ExecuteScalar();
                    }

                }

            }
            catch (Exception)
            {
                rol = "";
            }
            finally
            {
                con.Close();
            }
            return rol;
        }

        public int ObtenerRol(string rolaObtener)
        {
            int rol = 0;
            var con = new SqlConnection(stringConexion.cadena);

            try
            {

                using (con)
                {
                    var sql = "select Id from rol where responsabilidad = @rol";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@rol", rolaObtener);
                        con.Open();
                        rol = (int)cmd.ExecuteScalar();
                    }

                }

            }
            catch (Exception)
            {
                rol = 0;
            }
            finally
            {
                con.Close();
            }
            return rol;
        }

        public DataSet ObtenerUsuarios()
        {
            var con = new SqlConnection(stringConexion.cadena);
            var sql = "select u.Id as 'Cod Usuario', u.Nombre, u.Apellido, u.Dni, u.Domicilio, " +
                "u.Edad, r.Responsabilidad from Usuario u inner join " +
                "Login l on u.Id = l.Id_Usuario inner join Rol r on r.Id = U.Id_Rol where u.Estado = 'Activo'";
            SqlCommand cmd = new SqlCommand(sql, con);
            DataSet DS = new DataSet();

            try
            {

                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(cmd);

                DA.Fill(DS, "usuarios");

            }
            catch (Exception)
            {
                DS = null;
            }
            finally
            {
                con.Close();
            }
            return DS;
        }

        public int EditarUsuario(Entidades.Usuario usuarioAEditar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.EDITARUSUARIO", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", usuarioAEditar.Id);
                cmd.Parameters.AddWithValue("@Domicilio", usuarioAEditar.Domicilio);
                cmd.Parameters.AddWithValue("@Edad", usuarioAEditar.Edad);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public int EliminarUsuario(Entidades.Usuario usuarioAEliminar)
        {
            int respuesta = 0;
            try
            {
                objConnection = new SqlConnection(stringConexion.cadena);
                objConnection.Open();
                SqlCommand cmd = new SqlCommand("dbo.ELIMINARUSUARIO", objConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id_Usuario", usuarioAEliminar.Id);
                respuesta = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                respuesta = 0;
            }
            finally
            {
                objConnection.Close();
            }
            return respuesta;
        }

        public void DeleteUsuario(Entidades.Usuario usuarioAEliminar)
		{

		}

		public void SearchUsuario()
		{

		}

		public int LogearUsuario(string cuenta, string contrasenia)
		{
            int respuesta = 0;
            var con = new SqlConnection(stringConexion.cadena);

            try
			{
				
                using (con)
                {
                    var sql = "SELECT Id_Usuario FROM Login WHERE Cuenta = @Cuenta AND Contrasenia = @Contrasenia";
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@Cuenta", cuenta);
                        cmd.Parameters.AddWithValue("@Contrasenia", contrasenia);
                        con.Open();
                        respuesta = (int)cmd.ExecuteScalar();
                    }
                    
                }

            }
			catch(Exception)
			{
                respuesta = 0;
            }
            finally
            {
                con.Close();
            }
            return respuesta;
        }

	}
}
