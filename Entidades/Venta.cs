﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Venta
    {
        public int IdVenta { get; set; }
        public int IdProdcuto { get; set; }
        public decimal Cantidad { get; set; }
        public int IdUsuario { get; set; }
        public decimal MontoTotal { get; set; }
        public DateTime Fecha { get; set; }
        public string MedioPago { get; set; }
    }
}
