﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
   public class Stock
    {
        public int IdStock { get; set; }

        public string Descripcion { get; set; }

        public decimal Cantidad { get; set; }

        public int IdProveedor { get; set; }

        public int CantidadMinima { get; set; }

        public int IdUsuario { get; set; }

        public decimal Precio { get; set; }

    }
}
