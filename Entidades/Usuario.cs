﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class Usuario
	{
		public int Id { get; set; }
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public int Dni { get; set; }
		public string Domicilio { get; set; }
		public int  Edad { get; set; }
        public string Cuenta { get; set; }
        public string Contrasenia { get; set; }
    }
}
