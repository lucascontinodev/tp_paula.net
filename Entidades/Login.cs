﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
	public class Login:Usuario
	{

		public int Id_Login { get; set; }
		public int Id_Usuario { get; set; }
		public string Cuenta { get; set; }
		public string Contrasenia { get; set; }
        public string Rol { get; set; }

    }

}
