USE [HeladeriaEsperanza]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[Id_Cliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Dni] [varchar](50) NOT NULL,
	[Domicilio] [varchar](100) NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[Id_Cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Login]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Login](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Cuenta] [varchar](50) NOT NULL,
	[Contrasenia] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proveedores]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedores](
	[Id_proveedor] [int] IDENTITY(1,1) NOT NULL,
	[Cuit] [varchar](50) NOT NULL,
	[RazonSocial] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[Domicilio] [varchar](100) NOT NULL,
	[Estado] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Proveedores] PRIMARY KEY CLUSTERED 
(
	[Id_proveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](max) NOT NULL,
	[Responsabilidad] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stock]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stock](
	[Id_Stock] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[Cantidad] [decimal](18, 2) NOT NULL,
	[Id_Proveedor] [int] NOT NULL,
	[Cantidad_Minima] [int] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Estado] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[Id_Stock] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Rol] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Dni] [int] NOT NULL,
	[Domicilio] [varchar](100) NOT NULL,
	[Edad] [int] NOT NULL,
	[Estado] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ventas]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ventas](
	[Id_Venta] [int] NOT NULL,
	[Id_producto] [int] NOT NULL,
	[Cantidad] [decimal](18, 2) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[MontoTotal] [decimal](18, 2) NOT NULL,
	[Fecha] [date] NOT NULL,
	[Medio_Pago] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
	[Id_producto] ASC,
	[Id_Venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([Id_Cliente], [Nombre], [Apellido], [Dni], [Domicilio], [Telefono]) VALUES (1, N'lucas', N'contino', N'1452', N'asdas', N'12312')
INSERT [dbo].[Cliente] ([Id_Cliente], [Nombre], [Apellido], [Dni], [Domicilio], [Telefono]) VALUES (2, N'gaston', N'fernandez', N'2458', N'temperley', N'6666666')
SET IDENTITY_INSERT [dbo].[Cliente] OFF
SET IDENTITY_INSERT [dbo].[Login] ON 

INSERT [dbo].[Login] ([Id], [Id_Usuario], [Cuenta], [Contrasenia]) VALUES (2020, 1028, N'a', N'b')
SET IDENTITY_INSERT [dbo].[Login] OFF
SET IDENTITY_INSERT [dbo].[Proveedores] ON 

INSERT [dbo].[Proveedores] ([Id_proveedor], [Cuit], [RazonSocial], [Telefono], [email], [Domicilio], [Estado]) VALUES (1016, N'178936', N'Quilmes', N'1111', N'111', N'1111', N'Activo')
INSERT [dbo].[Proveedores] ([Id_proveedor], [Cuit], [RazonSocial], [Telefono], [email], [Domicilio], [Estado]) VALUES (1017, N'1789547874', N'La serenisima', N'222', N'111', N'1111', N'Activo')
SET IDENTITY_INSERT [dbo].[Proveedores] OFF
SET IDENTITY_INSERT [dbo].[Rol] ON 

INSERT [dbo].[Rol] ([Id], [Nombre], [Descripcion], [Responsabilidad]) VALUES (4, N'Leonardo Perez', N'ABMC Proveedores', N'Encargado')
INSERT [dbo].[Rol] ([Id], [Nombre], [Descripcion], [Responsabilidad]) VALUES (5, N'Pablo Garcia', N'ABMC Ventas', N'Cajero')
INSERT [dbo].[Rol] ([Id], [Nombre], [Descripcion], [Responsabilidad]) VALUES (6, N'Leonwl', N'AMBC Total', N'Gerente')
SET IDENTITY_INSERT [dbo].[Rol] OFF
SET IDENTITY_INSERT [dbo].[Stock] ON 

INSERT [dbo].[Stock] ([Id_Stock], [descripcion], [Cantidad], [Id_Proveedor], [Cantidad_Minima], [id_usuario], [Precio], [Estado]) VALUES (1015, N'Helado frutilla', CAST(15.00 AS Decimal(18, 2)), 1017, 20, 1028, CAST(70.00 AS Decimal(18, 2)), N'Activo')
INSERT [dbo].[Stock] ([Id_Stock], [descripcion], [Cantidad], [Id_Proveedor], [Cantidad_Minima], [id_usuario], [Precio], [Estado]) VALUES (1016, N'Taza', CAST(15.00 AS Decimal(18, 2)), 1016, 20, 1028, CAST(0.00 AS Decimal(18, 2)), N'Activo')
INSERT [dbo].[Stock] ([Id_Stock], [descripcion], [Cantidad], [Id_Proveedor], [Cantidad_Minima], [id_usuario], [Precio], [Estado]) VALUES (1017, N'Helado Frutilla', CAST(30.00 AS Decimal(18, 2)), 1016, 20, 1028, CAST(70.00 AS Decimal(18, 2)), N'Eliminado')
SET IDENTITY_INSERT [dbo].[Stock] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (13, 4, N'javier', N'ledesma', 12345, N'sdgdsg', 56, N'Eliminado')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1018, 5, N'lucas', N'contino', 2562, N'linier', 29, N'Activo')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1019, 4, N'carlos', N'gomez', 123, N'asdasd', 23, N'Activo')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1020, 5, N'lucas', N'contino', 1212, N'llllll', 23, N'Activo')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1021, 5, N'sadas', N'asdas', 231, N'fasfas', 67, N'Activo')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1022, 4, N'Lucas', N'Contino', 12345678, N'lavalle 312 Temperley', 18, N'Activo')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1023, 6, N'asd', N'ads', 5456, N'sd', 23, N'Activo')
INSERT [dbo].[Usuario] ([Id], [Id_Rol], [Nombre], [Apellido], [Dni], [Domicilio], [Edad], [Estado]) VALUES (1028, 4, N'Lucas', N'Contino', 12345678, N'Liniers', 29, N'Activo')
SET IDENTITY_INSERT [dbo].[Usuario] OFF
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (1, 1015, CAST(1.00 AS Decimal(18, 2)), 13, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Tarjeta')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (2, 1015, CAST(1.00 AS Decimal(18, 2)), 13, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (3, 1015, CAST(1.00 AS Decimal(18, 2)), 13, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (5, 1015, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(63.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (6, 1015, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Tarjeta')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (7, 1015, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(63.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Tarjeta')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (8, 1015, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (9, 1015, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (10, 1015, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (1, 1016, CAST(1.00 AS Decimal(18, 2)), 13, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Tarjeta')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (2, 1016, CAST(1.00 AS Decimal(18, 2)), 13, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (3, 1016, CAST(1.00 AS Decimal(18, 2)), 13, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (4, 1016, CAST(1.00 AS Decimal(18, 2)), 13, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (5, 1016, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (6, 1016, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Tarjeta')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (7, 1016, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Tarjeta')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (8, 1016, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (9, 1016, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (10, 1016, CAST(1.00 AS Decimal(18, 2)), 1028, CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
INSERT [dbo].[Ventas] ([Id_Venta], [Id_producto], [Cantidad], [id_usuario], [MontoTotal], [Fecha], [Medio_Pago]) VALUES (4, 1017, CAST(1.00 AS Decimal(18, 2)), 13, CAST(70.00 AS Decimal(18, 2)), CAST(N'2020-11-07' AS Date), N'Efectivo')
ALTER TABLE [dbo].[Login]  WITH CHECK ADD  CONSTRAINT [FK_Login_Usuario] FOREIGN KEY([Id_Usuario])
REFERENCES [dbo].[Usuario] ([Id])
GO
ALTER TABLE [dbo].[Login] CHECK CONSTRAINT [FK_Login_Usuario]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Proveedor] FOREIGN KEY([Id_Proveedor])
REFERENCES [dbo].[Proveedores] ([Id_proveedor])
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Producto_Proveedor]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Usuario] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuario] ([Id])
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Producto_Usuario]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol] FOREIGN KEY([Id_Rol])
REFERENCES [dbo].[Rol] ([Id])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Rol]
GO
ALTER TABLE [dbo].[Ventas]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Venta] FOREIGN KEY([Id_producto])
REFERENCES [dbo].[Stock] ([Id_Stock])
GO
ALTER TABLE [dbo].[Ventas] CHECK CONSTRAINT [FK_Producto_Venta]
GO
ALTER TABLE [dbo].[Ventas]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Usuario] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuario] ([Id])
GO
ALTER TABLE [dbo].[Ventas] CHECK CONSTRAINT [FK_Venta_Usuario]
GO
/****** Object:  StoredProcedure [dbo].[AUTHUSUARIO]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AUTHUSUARIO]
@Cuenta varchar(50),
@Contrasenia varchar(50)
AS
SELECT		l.Id_Usuario
FROM		DBO.LOGIN as l
WHERE		l.Cuenta = @Cuenta 
AND			l.Contrasenia = @Contrasenia
GO
/****** Object:  StoredProcedure [dbo].[DELETEROL]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DELETEROL]
@Id int
as
DELETE FROM DBO.ROL
WHERE		Id = @Id;

GO
/****** Object:  StoredProcedure [dbo].[EDITARCLIENTE]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EDITARCLIENTE]
@Id_Cliente int,
@Telefono varchar(50),
@Domicilio varchar(100)
AS
UPDATE	DBO.Cliente
SET	    Telefono = @Telefono,
		Domicilio = @Domicilio
WHERE
		Id_Cliente = @Id_Cliente;
GO
/****** Object:  StoredProcedure [dbo].[EDITARLOGIN]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EDITARLOGIN]
@Id_Usuario int,
@Cuenta varchar(50),
@Contrasenia varchar(50)
AS
UPDATE	DBO.Login
SET	    Cuenta = @Cuenta,
		Contrasenia = @Contrasenia
WHERE
		Id_Usuario = @Id_Usuario;
GO
/****** Object:  StoredProcedure [dbo].[EDITARPROVEEDOR]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EDITARPROVEEDOR]
@Id int,
@Telefono varchar(50),
@email varchar(50),
@Domicilio varchar(100)
AS
UPDATE	DBO.Proveedores
SET	    Telefono = @Telefono,
		email = @email,
		Domicilio = @Domicilio
WHERE
		Id_proveedor = @Id;
GO
/****** Object:  StoredProcedure [dbo].[EDITARSTOCK]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EDITARSTOCK]
@Id_Stock int,
@Cantidad decimal (18, 2),
@Cantidad_Minima int,
@Precio DECIMAL (18, 2),
@Id_Usuario varchar(50),
@Id_Proveedor int
AS
UPDATE	DBO.Stock
SET	    Cantidad = @Cantidad,
		Cantidad_Minima = @Cantidad_Minima,
		Precio = @Precio,
		id_usuario = @Id_Usuario,
		Id_Proveedor = @Id_Proveedor
WHERE
		Id_Stock = @Id_Stock;
GO
/****** Object:  StoredProcedure [dbo].[EDITARSTOCKPORVENTAS]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EDITARSTOCKPORVENTAS]
@Id_Ing1 int,
@Cant1 decimal (18,2)
AS
UPDATE	DBO.Stock
SET	    Cantidad = Cantidad - @Cant1
WHERE
		Id_Stock = @Id_Ing1;
GO
/****** Object:  StoredProcedure [dbo].[EDITARUSUARIO]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EDITARUSUARIO]
@Id int,
@Domicilio varchar(100),
@Edad int
AS
UPDATE	DBO.Usuario
SET	    Domicilio = @Domicilio,
		Edad = @Edad
WHERE
		Id = @Id;
GO
/****** Object:  StoredProcedure [dbo].[ELIMINARPROVEEDOR]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ELIMINARPROVEEDOR]
@Id int
AS
UPDATE	DBO.Proveedores
SET	    Estado = 'Eliminado'
WHERE
		Id_proveedor = @Id;
GO
/****** Object:  StoredProcedure [dbo].[ELIMINARSTOCK]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ELIMINARSTOCK]
@Id_Stock int
AS
UPDATE	DBO.Stock
SET	    Estado = 'Eliminado'
WHERE
		Id_Stock = @Id_Stock;
GO
/****** Object:  StoredProcedure [dbo].[ELIMINARUSUARIO]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ELIMINARUSUARIO]
@Id_Usuario int
as
DELETE Login
FROM Login l inner join Usuario u on u.Id = l.Id_Usuario
WHERE		Id_Usuario = @Id_Usuario;
UPDATE	DBO.Usuario
SET	    Estado = 'Eliminado'
WHERE
		Id = @Id_Usuario;
GO
/****** Object:  StoredProcedure [dbo].[INSERTCLIENTE]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTCLIENTE]
@Nombre varchar(50),
@Apellido varchar(50),
@Dni varchar(50),
@Domicilio varchar(100),
@Telefono varchar(50)
AS
declare @ultimo_id_cliente int;
INSERT INTO DBO.Cliente(
				Nombre,
				Apellido,
				Dni,
				Domicilio,
				Telefono)
		VALUES(
				@Nombre,
				@Apellido,
				@Dni,
				@Domicilio,
				@Telefono)

set @ultimo_id_cliente = SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[INSERTLOGIN]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[INSERTLOGIN]
@Id_Usuario int,
@Cuenta varchar(50),
@Contrasenia varchar(50)
AS
INSERT INTO DBO.LOGIN(
					Id_Usuario,
					Cuenta,
					Contrasenia
					)
			VALUES	(
					@Id_Usuario,
					@Cuenta,
					@Contrasenia
					)

GO
/****** Object:  StoredProcedure [dbo].[INSERTPROVEEDOR]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTPROVEEDOR]
@CUIT varchar(50),
@RazonSocial varchar(50),
@Telefono varchar(50),
@email varchar(50),
@Domicilio varchar(100)
AS
declare @ultimo_id_proveedor int;
INSERT INTO DBO.Proveedores(
				Cuit,
				RazonSocial,
				Telefono,
				email,
				Domicilio,
				Estado)
		VALUES(
				@CUIT,
				@RazonSocial,
				@Telefono,
				@email,
				@Domicilio,
				'Activo')

set @ultimo_id_proveedor = SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[INSERTROL]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTROL]
@Nombre varchar(50),
@Descripcion varchar(MAX),
@Responsabilidad varchar(MAX)
AS
INSERT INTO DBO.ROL(
				Nombre,
				Descripcion,
				Responsabilidad)
			VALUES(
				@Nombre,
				@Descripcion,
				@Responsabilidad)


GO
/****** Object:  StoredProcedure [dbo].[INSERTSTOCK]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTSTOCK]
@Descripcion varchar(200),
@Cantidad decimal (18, 2),
@Cantidad_Minima int,
@Id_Proveedor int,
@Id_Usuario int,
@Precio DECIMAL (18, 2)
AS
declare @ultimo_id_stock int;
INSERT INTO DBO.Stock(
				descripcion,
				Cantidad,
				Cantidad_Minima,
				Id_Proveedor,
				id_usuario,
				Precio,
				Estado)
		VALUES(
				@Descripcion,
				@Cantidad,
				@Cantidad_Minima,
				@Id_Proveedor,
				@Id_Usuario,
				@Precio,
				'Activo')

set @ultimo_id_stock = SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[INSERTUSUARIO]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTUSUARIO]
@Id_Rol int,
@Nombre varchar(50),
@Apellido varchar(50),
@Dni int,
@Domicilio varchar(50),
@Edad int,
@Cuenta varchar(50),
@Contrasenia varchar(50)
AS
declare @ultimo_id_usuario int;
INSERT INTO DBO.Usuario(
				Id_Rol,
				Nombre,
				Apellido,
				Dni,
				Domicilio,
				Edad,
				Estado)
		VALUES(
				@Id_Rol,
				@Nombre,
				@Apellido,
				@Dni,
				@Domicilio,
				@Edad,
				'Activo')

set @ultimo_id_usuario = SCOPE_IDENTITY();
EXEC DBO.INSERTLOGIN @ultimo_id_usuario, @Cuenta, @Contrasenia;
GO
/****** Object:  StoredProcedure [dbo].[INSERTVENTA]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[INSERTVENTA]
@Id_Venta int,
@Id_Producto int,
@Cantidad DECIMAL (18, 2),
@Id_Usuario int,
@Monto_Total DECIMAL (18, 2),
@Fecha date,
@Medio_Pago varchar(50)
AS
INSERT INTO DBO.Ventas(
				Id_Venta,
				Id_producto,
				Cantidad,
				id_usuario,
				MontoTotal,
				Fecha,
				Medio_Pago)
		VALUES(
				@Id_Venta,
				@Id_Producto,
				@Cantidad,
				@Id_Usuario,
				@Monto_Total,
				@Fecha,
				@Medio_Pago)
GO
/****** Object:  StoredProcedure [dbo].[SEARCHROL]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEARCHROL]
AS
SELECT	R.Nombre, R.Descripcion, R.Responsabilidad
FROM	DBO.ROL AS R

GO
/****** Object:  StoredProcedure [dbo].[UPDATEROL]    Script Date: 7/11/2020 15:29:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATEROL]
@Id int,
@Nombre varchar(50),
@Descripcion varchar(MAX),
@Responsabilidad varchar(MAX)
AS
UPDATE	DBO.ROL
SET	    Nombre = @Nombre,
		Descripcion = @Descripcion,
		Responsabilidad = @Responsabilidad
WHERE
		Id = @Id;

GO
