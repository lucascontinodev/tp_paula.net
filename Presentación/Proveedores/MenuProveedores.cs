﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Menu
{
    public partial class MenuProveedores : Form
    {
        private string rol;

        private int idUsuarioLogueado;
        public MenuProveedores(string rol, int idUsuarioLogueado)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuarioLogueado = idUsuarioLogueado;
        }

        private int idProveedor = 0;

        private void btnAlta_Click(object sender, EventArgs e)
        {

            if (txtCuit.Text == "")
            {
                MessageBox.Show("Ingrese un CUIT");
                return;
            }

            if (txtRazonSocial.Text == "")
            {
                MessageBox.Show("Ingrese una Razon Social");
                return;
            }

            if (txtTelefono.Text == "")
            {
                MessageBox.Show("Ingrese un Telefono");
                return;
            }

            if (txtEmail.Text == "")
            {
                MessageBox.Show("Ingrese un Email");
                return;
            }

            if (txtDireccion.Text == "")
            {
                MessageBox.Show("Ingrese una Direccion");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea agregar este proveedor?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            {
                Entidades.Proveedor objEntidadProveedor = new Entidades.Proveedor();
                objEntidadProveedor.Cuit = txtCuit.Text;
                objEntidadProveedor.Domicilio = txtDireccion.Text;
                objEntidadProveedor.RazonSocial = txtRazonSocial.Text;
                objEntidadProveedor.Telefono = txtTelefono.Text;
                objEntidadProveedor.Email = txtEmail.Text;
                Negocio.Proveedor objNegocioPorveedor = new Negocio.Proveedor();
                string respuesta = objNegocioPorveedor.InsertProveedor(objEntidadProveedor);
                if (respuesta == "OK")
                {
                    MessageBox.Show("Proveedor ingresado con exito");
                    MenuProveedores_Load(sender, e);
                }
                else
                {
                    MessageBox.Show(respuesta);
                }
            }
        }

        private void MenuProveedores_Load(object sender, EventArgs e)
        {
            rbtAlta.Checked = true;

            DataSet lsProveedor = new DataSet();
            Negocio.Proveedor listaProveedores = new Negocio.Proveedor();
            lsProveedor = listaProveedores.ObtenerProveedores();

            // llenamos grilla
            grillaProveedor.DataSource = lsProveedor;
            grillaProveedor.DataMember = "proveedores";

            // llenamos combo de codigo
            //cboCodigo.DataSource = lsProveedor.Copy("proveedores");
            //cboCodigo.DisplayMember = "Id_proveedor";
            //cboCodigo.ValueMember = "Id_proveedor";

           
        }

        private void grillaProveedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void grillaProveedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Update the labels to reflect changes to the selection.
            MessageBox.Show(grillaProveedor.SelectedRows.ToString());
        }

        private void grillaProveedor_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtId.Text = grillaProveedor.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtCuit.Text = (string)grillaProveedor.Rows[e.RowIndex].Cells[1].Value;
                txtRazonSocial.Text = (string)grillaProveedor.Rows[e.RowIndex].Cells[2].Value;
                txtTelefono.Text = (string)grillaProveedor.Rows[e.RowIndex].Cells[3].Value;
                txtEmail.Text = (string)grillaProveedor.Rows[e.RowIndex].Cells[4].Value;
                txtDireccion.Text = (string)grillaProveedor.Rows[e.RowIndex].Cells[5].Value;
            } catch (Exception)
            {
                
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBaja_Click(object sender, EventArgs e)
        {

            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese el ID del Proveedor");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea eliminar este proveedor?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            Entidades.Proveedor IdProveedor = new Entidades.Proveedor();
            IdProveedor.Id_proveedor = Convert.ToInt32(txtId.Text);

            Negocio.Proveedor proveedor = new Negocio.Proveedor();
            int respuesta = proveedor.EliminarProveedor(IdProveedor);

            if (respuesta > 0)
            {
                MessageBox.Show("Proveedor eliminado");
                MenuProveedores_Load(sender, e);
            } else
            {
                MessageBox.Show("error al intentar borrar");
            }

        }

        private void btnModificacion_Click(object sender, EventArgs e)
        {



            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese un ID de proveedor");
                return;
            }

            if (txtTelefono.Text == "")
            {
                MessageBox.Show("Ingrese un Telefono");
                return;
            }

            if (txtEmail.Text == "")
            {
                MessageBox.Show("Ingrese un Email");
                return;
            }

            if (txtDireccion.Text == "")
            {
                MessageBox.Show("Ingrese una Direccion");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea modificar este proveedor?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            Entidades.Proveedor proveedorEditar = new Entidades.Proveedor();
            proveedorEditar.Id_proveedor = Convert.ToInt32(txtId.Text);
            proveedorEditar.Email = txtEmail.Text;
            proveedorEditar.Telefono = txtTelefono.Text;
            proveedorEditar.Domicilio = txtDireccion.Text;


            Negocio.Proveedor proveedor = new Negocio.Proveedor();
            int respuesta = proveedor.EditarProveedor(proveedorEditar);

            if (respuesta > 0)
            {
                MessageBox.Show("Proveedor editado");
                MenuProveedores_Load(sender, e);
            }
            else
            {
                MessageBox.Show("error al intentar borrar");
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MenuGerenal menuGeneral = new MenuGerenal(rol, idUsuarioLogueado);
            this.Hide();
            menuGeneral.Show();
        }

        private void rbtAlta_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtAlta.Checked)
            {
                txtCuit.Enabled = true;
                txtRazonSocial.Enabled = true;
                txtTelefono.Enabled = true;
                txtEmail.Enabled = true;
                txtDireccion.Enabled = true;
                txtId.Enabled = false;
                btnAlta.Enabled = true;
                btnModificacion.Enabled = false;
                btnBaja.Enabled = false;
            }
        }

        private void rbtModificar_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtModificar.Checked)
            {
                txtCuit.Enabled = false;
                txtRazonSocial.Enabled = false;
                txtTelefono.Enabled = true;
                txtEmail.Enabled = true;
                txtDireccion.Enabled = true;
                txtId.Enabled = true;
                btnAlta.Enabled = false;
                btnModificacion.Enabled = true;
                btnBaja.Enabled = false;
            }
        }

        private void rbtBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtBaja.Checked)
            {
                txtCuit.Enabled = false;
                txtRazonSocial.Enabled = false;
                txtTelefono.Enabled = false;
                txtEmail.Enabled = false;
                txtDireccion.Enabled = false;
                txtId.Enabled = true;
                btnAlta.Enabled = false;
                btnModificacion.Enabled = false;
                btnBaja.Enabled = true;
            }
        }
    }
}
