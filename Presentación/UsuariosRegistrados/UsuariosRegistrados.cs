﻿using Presentación.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.UsuariosRegistrados
{
    public partial class UsuariosRegistrados : Form
    {
        private string rol;

        private int idUsuarioLogueado;
        public UsuariosRegistrados(string rol, int idUsuarioLogueado)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuarioLogueado = idUsuarioLogueado;
        }

        private int idUsuario = 0;

        private void UsuariosRegistrados_Load(object sender, EventArgs e)
        {
            rbtAlta.Checked = true;
            btnModificacion.Enabled = false;
            btnBaja.Enabled = false;
           
            DataSet datosUsuarios = new DataSet();
            Negocio.Usuario listaUsuarios = new Negocio.Usuario();
            datosUsuarios = listaUsuarios.ObtenerUsuarios();

            // llenamos grilla
            grillaUsuarios.DataSource = datosUsuarios;
            grillaUsuarios.DataMember = "usuarios";
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MenuGerenal menuGeneral = new MenuGerenal(rol, idUsuarioLogueado);
            this.Hide();
            menuGeneral.Show();
        }

        private void txtDNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {

            if (comboRol.Text == "")
            {
                MessageBox.Show("Seleccione un Rol");
                return;
            }

            if (txtNombre.Text == "")
            {
                MessageBox.Show("Ingrese un Nombre");
                return;
            }

            if (txtApellido.Text == "")
            {
                MessageBox.Show("Ingrese un Apellido");
                return;
            }

            if (txtDni.Text == "")
            {
                MessageBox.Show("Ingrese un DNI");
                return;
            }

            if (txtDomicilio.Text == "")
            {
                MessageBox.Show("Ingrese un Domicilio");
                return;
            }

            if (txtEdad.Text == "")
            {
                MessageBox.Show("Ingrese una Edad");
                return;
            }

            if (txtCuenta.Text == "")
            {
                MessageBox.Show("Ingrese una Cuenta");
                return;
            }

            if (txtContrasenia.Text == "")
            {
                MessageBox.Show("Ingrese una Contraseña");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea agregar este usuario?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            Entidades.Login objEntidadLogin = new Entidades.Login();
            objEntidadLogin.Apellido = txtApellido.Text;
            objEntidadLogin.Contrasenia = txtContrasenia.Text;
            objEntidadLogin.Cuenta = txtCuenta.Text;
            objEntidadLogin.Rol = comboRol.Text;
            objEntidadLogin.Dni = int.Parse(txtDni.Text);
            objEntidadLogin.Domicilio = txtDomicilio.Text;
            objEntidadLogin.Edad = int.Parse(txtEdad.Text);
            objEntidadLogin.Nombre = txtNombre.Text;

            //consultamos rol a insertar
            Negocio.Usuario rol = new Negocio.Usuario();
            int rolObtenido = rol.ObtenerRol(objEntidadLogin.Rol);

            if (rolObtenido == 0)
            {
                MessageBox.Show("error al obtener el rol");
                return;
            }

            Negocio.Usuario objNegocioUsuario = new Negocio.Usuario();
            string respuesta = objNegocioUsuario.InsertUsuario(objEntidadLogin, rolObtenido);
            if (respuesta == "OK")
            {
                // this.Hide();
                MessageBox.Show("Usuario ingresado con exito");
                UsuariosRegistrados_Load(sender, e);
            }
            else
            {
                MessageBox.Show(respuesta);
            }
        }

        private void grillaUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtId.Text = grillaUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtNombre.Text = grillaUsuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtApellido.Text = grillaUsuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtDni.Text = grillaUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtDomicilio.Text = grillaUsuarios.Rows[e.RowIndex].Cells[4].Value.ToString();
                txtEdad.Text = grillaUsuarios.Rows[e.RowIndex].Cells[5].Value.ToString();
                //txtCuenta.Text = grillaUsuarios.Rows[e.RowIndex].Cells[6].Value.ToString();
                //txtContrasenia.Text = grillaUsuarios.Rows[e.RowIndex].Cells[7].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void btnModificacion_Click(object sender, EventArgs e)
        {

            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese el ID del usuario a modificar");
                return;
            }

            if (txtDomicilio.Text == "")
            {
                MessageBox.Show("Ingrese un Domicilio");
                return;
            }

            if (txtEdad.Text == "")
            {
                MessageBox.Show("Ingrese una Edad");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea modificar este usuario?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }


            Entidades.Usuario usuarioEditar = new Entidades.Usuario();
            usuarioEditar.Id = Convert.ToInt32(txtId.Text);
            usuarioEditar.Domicilio = txtDomicilio.Text;
            usuarioEditar.Edad = Convert.ToInt32(txtEdad.Text);


            Negocio.Usuario usuario = new Negocio.Usuario();
            int respuesta = usuario.EditarUsuario(usuarioEditar);

            if (respuesta > 0)
            {
                MessageBox.Show("Usuario editado");
                UsuariosRegistrados_Load(sender, e);
            }
            else
            {
                MessageBox.Show("error al intentar editar");
            }
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {

            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese el ID del usuario a eliminar");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea eliminar este usuario?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            Entidades.Usuario UsuarioId = new Entidades.Usuario();
            UsuarioId.Id = Convert.ToInt32(txtId.Text);

            Negocio.Usuario usuario = new Negocio.Usuario();
            int respuesta = usuario.EliminarUsuario(UsuarioId);

            if (respuesta > 0)
            {
                MessageBox.Show("Usuario eliminado");
                UsuariosRegistrados_Load(sender, e);
            }
            else
            {
                MessageBox.Show("error al intentar borrar");
            }
        }

        private void rbtModificar_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtModificar.Checked)
            {
                txtApellido.Enabled = false;
                txtContrasenia.Enabled = false;
                txtCuenta.Enabled = false;
                txtDni.Enabled = false;
                txtDomicilio.Enabled = true;
                txtEdad.Enabled = true;
                txtId.Enabled = true;
                txtNombre.Enabled = false;
                btnAlta.Enabled = false;
                btnModificacion.Enabled = true;
                btnBaja.Enabled = false;
            }
        }

        private void rbtAlta_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtAlta.Checked)
            {
                txtApellido.Enabled = true;
                txtContrasenia.Enabled = true;
                txtCuenta.Enabled = true;
                txtDni.Enabled = true;
                txtDomicilio.Enabled = true;
                txtEdad.Enabled = true;
                txtId.Enabled = false;
                txtNombre.Enabled = true;
                btnAlta.Enabled = true;
                btnModificacion.Enabled = false;
                btnBaja.Enabled = false;
            }
        }

        private void rbtBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtBaja.Checked)
            {
                txtApellido.Enabled = false;
                txtContrasenia.Enabled = false;
                txtCuenta.Enabled = false;
                txtDni.Enabled = false;
                txtDomicilio.Enabled = false;
                txtEdad.Enabled = false;
                txtId.Enabled = true;
                txtNombre.Enabled = false;
                btnAlta.Enabled = false;
                btnModificacion.Enabled = false;
                btnBaja.Enabled = true;
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }
    }
}
