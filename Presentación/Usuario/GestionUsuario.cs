﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Usuario
{
	public partial class GestionUsuario : Form
	{
		public GestionUsuario()
		{
			InitializeComponent();
		}

		private void btnAñadir_Click(object sender, EventArgs e)
		{
			if (comboRol.Text == "")
            {
				MessageBox.Show("Seleccione un Rol");
				return;
			}

			if (txtNombre.Text == "")
			{
				MessageBox.Show("Ingrese un Nombre");
				return;
			}

			if (txtApellido.Text == "")
			{
				MessageBox.Show("Ingrese un Apellido");
				return;
			}

			if (txtDni.Text == "")
			{
				MessageBox.Show("Ingrese un DNI");
				return;
			}

			if (txtDomicilio.Text == "")
			{
				MessageBox.Show("Ingrese un Domicilio");
				return;
			}

			if (txtEdad.Text == "")
			{
				MessageBox.Show("Ingrese una Edad");
				return;
			}

			if (txtCuenta.Text == "")
			{
				MessageBox.Show("Ingrese una Cuenta");
				return;
			}

			if (txtContrasenia.Text == "")
			{
				MessageBox.Show("Ingrese una Contraseña");
				return;
			}



			DialogResult dialogResult = MessageBox.Show("¿Desea agregar este usuario?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
			if (dialogResult == DialogResult.No)
			{
				return;
			}

			Entidades.Login objEntidadLogin = new Entidades.Login();
			objEntidadLogin.Apellido = txtApellido.Text;
			objEntidadLogin.Contrasenia = txtContrasenia.Text;
			objEntidadLogin.Cuenta = txtCuenta.Text;
            objEntidadLogin.Rol = comboRol.Text;
            objEntidadLogin.Dni = int.Parse(txtDni.Text);
			objEntidadLogin.Domicilio = txtDomicilio.Text;
			objEntidadLogin.Edad = int.Parse(txtEdad.Text);
			objEntidadLogin.Nombre = txtNombre.Text;

			//consultamos rol a insertar
			Negocio.Usuario rol = new Negocio.Usuario();
			int rolObtenido = rol.ObtenerRol(objEntidadLogin.Rol);

			if (rolObtenido == 0)
            {
				MessageBox.Show("error al obtener el rol");
				return;
			}

			Negocio.Usuario objNegocioUsuario = new Negocio.Usuario();
			string respuesta = objNegocioUsuario.InsertUsuario(objEntidadLogin, rolObtenido);
            if(respuesta == "OK")
            {
                // this.Hide();
				MessageBox.Show("Usuario ingresado con exito");
			}
            else
            {
                MessageBox.Show(respuesta);
            }
		}

		private void InsertUsuario_Load(object sender, EventArgs e)
		{

		}

		private void txtDNI_KeyPress(object sender, KeyPressEventArgs e)
		{
			//Para obligar a que sólo se introduzcan números
			if (Char.IsDigit(e.KeyChar))
			{
				e.Handled = false;
			}
			else
			  if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
			{
				e.Handled = false;
			}
			else
			{
				//el resto de teclas pulsadas se desactivan
				e.Handled = true;
			}
		}

		private void label7_Click(object sender, EventArgs e)
		{

		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{

		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}

		private void label8_Click(object sender, EventArgs e)
		{

		}

        private void button1_Click(object sender, EventArgs e)
        {
			Login.Login menuLogin = new Login.Login();
			this.Hide();
			menuLogin.Show();
		}

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
			txtApellido.Text = "";
			txtContrasenia.Text = "";
			txtCuenta.Text = "";
			txtDni.Text = "";
			txtDomicilio.Text = "";
			txtEdad.Text = "";
			txtNombre.Text = "";
		}

        private void txtDni_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
