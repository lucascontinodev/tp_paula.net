﻿using Presentación.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Pedidos
{
    public partial class Venta : Form
    {
        private string rol;

        private int idUsuarioLogueado;
        public Venta(string rol, int idUsuarioLogueado)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuarioLogueado = idUsuarioLogueado;
        }

        private string parametroFiltro = "";

        private string filtrarPor = "";

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MenuGerenal menuGeneral = new MenuGerenal(rol, idUsuarioLogueado);
            this.Hide();
            menuGeneral.Show();
        }

        private void Venta_Load(object sender, EventArgs e)
        {
            
        }

        private void rbtIdVenta_CheckedChanged(object sender, EventArgs e)
        {
           
            if (rbtIdVenta.Checked)
            {
                DataSet venta = new DataSet();
                Negocio.Venta datosVenta = new Negocio.Venta();
                venta = datosVenta.ObtenerVentas();

                comboTipoFiltro.DataSource = venta.Tables[0];
                comboTipoFiltro.DisplayMember = "Id_Venta";
                comboTipoFiltro.ValueMember = "Id_Venta";
                filtrarPor = "IdVenta";
            }
        }

        private void rbtMedioPago_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtMedioPago.Checked)
            {
                DataSet venta = new DataSet();
                Negocio.Venta datosVenta = new Negocio.Venta();
                venta = datosVenta.ObtenerMedioPago();

                comboTipoFiltro.DataSource = venta.Tables[0];
                comboTipoFiltro.DisplayMember = "Medio_pago";
                comboTipoFiltro.ValueMember = "Medio_pago";
                filtrarPor = "MedioPago";
            }
        }

        private void rbtUsuario_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtUsuario.Checked)
            {
                DataSet venta = new DataSet();
                Negocio.Venta datosVenta = new Negocio.Venta();
                venta = datosVenta.ObtenerUsuario();

                comboTipoFiltro.DataSource = venta.Tables[0];
                comboTipoFiltro.DisplayMember = "Usuario";
                comboTipoFiltro.ValueMember = "Usuario";
                filtrarPor = "Usuario";
            }
        }

        private void rbtDescripcionProducto_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtDescripcionProducto.Checked)
            {
                DataSet venta = new DataSet();
                Negocio.Venta datosVenta = new Negocio.Venta();
                venta = datosVenta.ObtenerProducto();

                comboTipoFiltro.DataSource = venta.Tables[0];
                comboTipoFiltro.DisplayMember = "descripcion";
                comboTipoFiltro.ValueMember = "descripcion";
                filtrarPor = "Producto";
            }
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            
            if (comboTipoFiltro.SelectedValue == null)
            {
                MessageBox.Show("Seleccione un valor de la lista");
                return;
            }
            parametroFiltro = comboTipoFiltro.SelectedValue.ToString();

            DataSet venta = new DataSet();
            Negocio.Venta datosVenta = new Negocio.Venta();

            switch (filtrarPor)
            {
                case "IdVenta":
                    venta = datosVenta.ObtenerListaPorId(Convert.ToInt32(parametroFiltro));
                    grillaVentas.DataSource = venta;
                    grillaVentas.DataMember = "ListaPorId";
                    break;
                case "MedioPago":
                    venta = datosVenta.ObtenerListaPorMedioPago(parametroFiltro);
                    grillaVentas.DataSource = venta;
                    grillaVentas.DataMember = "ListaPorMedioPago";
                    break;
                case "Usuario":
                    venta = datosVenta.ObtenerListaPorUsuario(parametroFiltro);
                    grillaVentas.DataSource = venta;
                    grillaVentas.DataMember = "ListaPorUsuario";
                    break;
                case "Producto":
                    venta = datosVenta.ObtenerListaPorProducto(parametroFiltro);
                    grillaVentas.DataSource = venta;
                    grillaVentas.DataMember = "ListaPorProducto";
                    break;
                default:
                    MessageBox.Show("Seleccione una opcion para filtrar");
                    break;
            }
                
        }

        private void comboTipoFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboTipoFiltro.Items.Count == 0)
            {
                MessageBox.Show("Seleccione una opcion");
                return;
            }
        }
    }
}
