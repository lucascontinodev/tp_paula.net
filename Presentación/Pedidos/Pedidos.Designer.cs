﻿namespace Presentación.Pedidos
{
    partial class Pedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboIng5 = new System.Windows.Forms.ComboBox();
            this.comboIng4 = new System.Windows.Forms.ComboBox();
            this.comboIng3 = new System.Windows.Forms.ComboBox();
            this.comboIng2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCantida2 = new System.Windows.Forms.TextBox();
            this.txtCantida3 = new System.Windows.Forms.TextBox();
            this.txtCantida4 = new System.Windows.Forms.TextBox();
            this.txtCantida5 = new System.Windows.Forms.TextBox();
            this.txtCantida1 = new System.Windows.Forms.TextBox();
            this.comboIng1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.cheIng3 = new System.Windows.Forms.CheckBox();
            this.cheIng4 = new System.Windows.Forms.CheckBox();
            this.cheIng5 = new System.Windows.Forms.CheckBox();
            this.btnVolver = new System.Windows.Forms.Button();
            this.grillaProductosPedidos = new System.Windows.Forms.DataGridView();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.btnNuevaVenta = new System.Windows.Forms.Button();
            this.rbtEfectivo = new System.Windows.Forms.RadioButton();
            this.rbtTarjeta = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.lstPuntoCritico = new System.Windows.Forms.ListBox();
            this.lblPuntoCritico = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductosPedidos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DNI Cliente";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(275, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cantidad";
            // 
            // comboIng5
            // 
            this.comboIng5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIng5.FormattingEnabled = true;
            this.comboIng5.Location = new System.Drawing.Point(113, 132);
            this.comboIng5.Name = "comboIng5";
            this.comboIng5.Size = new System.Drawing.Size(121, 21);
            this.comboIng5.TabIndex = 15;
            // 
            // comboIng4
            // 
            this.comboIng4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIng4.FormattingEnabled = true;
            this.comboIng4.Location = new System.Drawing.Point(113, 105);
            this.comboIng4.Name = "comboIng4";
            this.comboIng4.Size = new System.Drawing.Size(121, 21);
            this.comboIng4.TabIndex = 16;
            // 
            // comboIng3
            // 
            this.comboIng3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIng3.FormattingEnabled = true;
            this.comboIng3.Location = new System.Drawing.Point(113, 78);
            this.comboIng3.Name = "comboIng3";
            this.comboIng3.Size = new System.Drawing.Size(121, 21);
            this.comboIng3.TabIndex = 17;
            // 
            // comboIng2
            // 
            this.comboIng2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIng2.FormattingEnabled = true;
            this.comboIng2.Location = new System.Drawing.Point(113, 51);
            this.comboIng2.Name = "comboIng2";
            this.comboIng2.Size = new System.Drawing.Size(121, 21);
            this.comboIng2.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Sabor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Sabor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(41, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Sabor";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Sabor";
            // 
            // txtCantida2
            // 
            this.txtCantida2.Location = new System.Drawing.Point(250, 51);
            this.txtCantida2.Name = "txtCantida2";
            this.txtCantida2.Size = new System.Drawing.Size(100, 20);
            this.txtCantida2.TabIndex = 25;
            this.txtCantida2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // txtCantida3
            // 
            this.txtCantida3.Location = new System.Drawing.Point(250, 79);
            this.txtCantida3.Name = "txtCantida3";
            this.txtCantida3.Size = new System.Drawing.Size(100, 20);
            this.txtCantida3.TabIndex = 26;
            this.txtCantida3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // txtCantida4
            // 
            this.txtCantida4.Location = new System.Drawing.Point(250, 106);
            this.txtCantida4.Name = "txtCantida4";
            this.txtCantida4.Size = new System.Drawing.Size(100, 20);
            this.txtCantida4.TabIndex = 27;
            this.txtCantida4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // txtCantida5
            // 
            this.txtCantida5.Location = new System.Drawing.Point(250, 132);
            this.txtCantida5.Name = "txtCantida5";
            this.txtCantida5.Size = new System.Drawing.Size(100, 20);
            this.txtCantida5.TabIndex = 28;
            this.txtCantida5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // txtCantida1
            // 
            this.txtCantida1.Location = new System.Drawing.Point(250, 24);
            this.txtCantida1.Name = "txtCantida1";
            this.txtCantida1.Size = new System.Drawing.Size(100, 20);
            this.txtCantida1.TabIndex = 30;
            this.txtCantida1.TextChanged += new System.EventHandler(this.txtCantida1_TextChanged);
            this.txtCantida1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // comboIng1
            // 
            this.comboIng1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboIng1.FormattingEnabled = true;
            this.comboIng1.Location = new System.Drawing.Point(113, 24);
            this.comboIng1.Name = "comboIng1";
            this.comboIng1.Size = new System.Drawing.Size(121, 21);
            this.comboIng1.TabIndex = 29;
            this.comboIng1.SelectedIndexChanged += new System.EventHandler(this.comboIng1_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Tipo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(356, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "Unidades";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(356, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "Kilos";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(356, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Kilos";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(356, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Kilos";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(356, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Kilos";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(275, 204);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 37;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // cheIng3
            // 
            this.cheIng3.AutoSize = true;
            this.cheIng3.Location = new System.Drawing.Point(7, 85);
            this.cheIng3.Name = "cheIng3";
            this.cheIng3.Size = new System.Drawing.Size(15, 14);
            this.cheIng3.TabIndex = 38;
            this.cheIng3.UseVisualStyleBackColor = true;
            this.cheIng3.CheckedChanged += new System.EventHandler(this.cheIng3_CheckedChanged);
            // 
            // cheIng4
            // 
            this.cheIng4.AutoSize = true;
            this.cheIng4.Location = new System.Drawing.Point(7, 112);
            this.cheIng4.Name = "cheIng4";
            this.cheIng4.Size = new System.Drawing.Size(15, 14);
            this.cheIng4.TabIndex = 39;
            this.cheIng4.UseVisualStyleBackColor = true;
            this.cheIng4.CheckedChanged += new System.EventHandler(this.cheIng4_CheckedChanged);
            // 
            // cheIng5
            // 
            this.cheIng5.AutoSize = true;
            this.cheIng5.Location = new System.Drawing.Point(7, 138);
            this.cheIng5.Name = "cheIng5";
            this.cheIng5.Size = new System.Drawing.Size(15, 14);
            this.cheIng5.TabIndex = 40;
            this.cheIng5.UseVisualStyleBackColor = true;
            this.cheIng5.CheckedChanged += new System.EventHandler(this.cheIng5_CheckedChanged);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(713, 432);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 54;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // grillaProductosPedidos
            // 
            this.grillaProductosPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaProductosPedidos.Location = new System.Drawing.Point(448, 12);
            this.grillaProductosPedidos.Name = "grillaProductosPedidos";
            this.grillaProductosPedidos.Size = new System.Drawing.Size(340, 352);
            this.grillaProductosPedidos.TabIndex = 55;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Location = new System.Drawing.Point(685, 385);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(103, 23);
            this.btnConfirmar.TabIndex = 56;
            this.btnConfirmar.Text = "Confirmar Venta";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // btnNuevaVenta
            // 
            this.btnNuevaVenta.Location = new System.Drawing.Point(448, 385);
            this.btnNuevaVenta.Name = "btnNuevaVenta";
            this.btnNuevaVenta.Size = new System.Drawing.Size(103, 23);
            this.btnNuevaVenta.TabIndex = 57;
            this.btnNuevaVenta.Text = "Nueva Venta";
            this.btnNuevaVenta.UseVisualStyleBackColor = true;
            this.btnNuevaVenta.Click += new System.EventHandler(this.btnNuevaVenta_Click);
            // 
            // rbtEfectivo
            // 
            this.rbtEfectivo.AutoSize = true;
            this.rbtEfectivo.Location = new System.Drawing.Point(7, 249);
            this.rbtEfectivo.Name = "rbtEfectivo";
            this.rbtEfectivo.Size = new System.Drawing.Size(64, 17);
            this.rbtEfectivo.TabIndex = 0;
            this.rbtEfectivo.TabStop = true;
            this.rbtEfectivo.Text = "Efectivo";
            this.rbtEfectivo.UseVisualStyleBackColor = true;
            // 
            // rbtTarjeta
            // 
            this.rbtTarjeta.AutoSize = true;
            this.rbtTarjeta.Location = new System.Drawing.Point(7, 210);
            this.rbtTarjeta.Name = "rbtTarjeta";
            this.rbtTarjeta.Size = new System.Drawing.Size(58, 17);
            this.rbtTarjeta.TabIndex = 1;
            this.rbtTarjeta.TabStop = true;
            this.rbtTarjeta.Text = "Tarjeta";
            this.rbtTarjeta.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "Medio de Pago";
            // 
            // txtDni
            // 
            this.txtDni.Location = new System.Drawing.Point(134, 207);
            this.txtDni.Name = "txtDni";
            this.txtDni.Size = new System.Drawing.Size(100, 20);
            this.txtDni.TabIndex = 60;
            this.txtDni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDNI_KeyPress);
            // 
            // lstPuntoCritico
            // 
            this.lstPuntoCritico.FormattingEnabled = true;
            this.lstPuntoCritico.Location = new System.Drawing.Point(7, 313);
            this.lstPuntoCritico.Name = "lstPuntoCritico";
            this.lstPuntoCritico.Size = new System.Drawing.Size(435, 95);
            this.lstPuntoCritico.TabIndex = 61;
            // 
            // lblPuntoCritico
            // 
            this.lblPuntoCritico.AutoSize = true;
            this.lblPuntoCritico.Location = new System.Drawing.Point(4, 297);
            this.lblPuntoCritico.Name = "lblPuntoCritico";
            this.lblPuntoCritico.Size = new System.Drawing.Size(138, 13);
            this.lblPuntoCritico.TabIndex = 62;
            this.lblPuntoCritico.Text = "Productos con Stock critico";
            // 
            // Pedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 467);
            this.Controls.Add(this.lblPuntoCritico);
            this.Controls.Add(this.lstPuntoCritico);
            this.Controls.Add(this.txtDni);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtTarjeta);
            this.Controls.Add(this.rbtEfectivo);
            this.Controls.Add(this.btnNuevaVenta);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.grillaProductosPedidos);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.cheIng5);
            this.Controls.Add(this.cheIng4);
            this.Controls.Add(this.cheIng3);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCantida1);
            this.Controls.Add(this.comboIng1);
            this.Controls.Add(this.txtCantida5);
            this.Controls.Add(this.txtCantida4);
            this.Controls.Add(this.txtCantida3);
            this.Controls.Add(this.txtCantida2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboIng2);
            this.Controls.Add(this.comboIng3);
            this.Controls.Add(this.comboIng4);
            this.Controls.Add(this.comboIng5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Name = "Pedidos";
            this.Text = "Pedidos";
            this.Load += new System.EventHandler(this.Pedidos_Load);
            this.Shown += new System.EventHandler(this.Pedidos_Mounted);
            ((System.ComponentModel.ISupportInitialize)(this.grillaProductosPedidos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboIng5;
        private System.Windows.Forms.ComboBox comboIng4;
        private System.Windows.Forms.ComboBox comboIng3;
        private System.Windows.Forms.ComboBox comboIng2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCantida2;
        private System.Windows.Forms.TextBox txtCantida3;
        private System.Windows.Forms.TextBox txtCantida4;
        private System.Windows.Forms.TextBox txtCantida5;
        private System.Windows.Forms.TextBox txtCantida1;
        private System.Windows.Forms.ComboBox comboIng1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.CheckBox cheIng3;
        private System.Windows.Forms.CheckBox cheIng4;
        private System.Windows.Forms.CheckBox cheIng5;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.DataGridView grillaProductosPedidos;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnNuevaVenta;
        private System.Windows.Forms.RadioButton rbtEfectivo;
        private System.Windows.Forms.RadioButton rbtTarjeta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.ListBox lstPuntoCritico;
        private System.Windows.Forms.Label lblPuntoCritico;
    }
}