﻿namespace Presentación.Pedidos
{
    partial class Venta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboTipoFiltro = new System.Windows.Forms.ComboBox();
            this.rbtIdVenta = new System.Windows.Forms.RadioButton();
            this.rbtMedioPago = new System.Windows.Forms.RadioButton();
            this.rbtUsuario = new System.Windows.Forms.RadioButton();
            this.rbtDescripcionProducto = new System.Windows.Forms.RadioButton();
            this.grillaVentas = new System.Windows.Forms.DataGridView();
            this.btnVer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grillaVentas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(701, 394);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(75, 23);
            this.btnVolver.TabIndex = 18;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Ver detalle de venta segun:";
            // 
            // comboTipoFiltro
            // 
            this.comboTipoFiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTipoFiltro.FormattingEnabled = true;
            this.comboTipoFiltro.Location = new System.Drawing.Point(41, 194);
            this.comboTipoFiltro.Name = "comboTipoFiltro";
            this.comboTipoFiltro.Size = new System.Drawing.Size(121, 21);
            this.comboTipoFiltro.TabIndex = 20;
            this.comboTipoFiltro.SelectedIndexChanged += new System.EventHandler(this.comboTipoFiltro_SelectedIndexChanged);
            // 
            // rbtIdVenta
            // 
            this.rbtIdVenta.AutoSize = true;
            this.rbtIdVenta.Location = new System.Drawing.Point(41, 82);
            this.rbtIdVenta.Name = "rbtIdVenta";
            this.rbtIdVenta.Size = new System.Drawing.Size(83, 17);
            this.rbtIdVenta.TabIndex = 21;
            this.rbtIdVenta.TabStop = true;
            this.rbtIdVenta.Text = "N° de Venta";
            this.rbtIdVenta.UseVisualStyleBackColor = true;
            this.rbtIdVenta.CheckedChanged += new System.EventHandler(this.rbtIdVenta_CheckedChanged);
            // 
            // rbtMedioPago
            // 
            this.rbtMedioPago.AutoSize = true;
            this.rbtMedioPago.Location = new System.Drawing.Point(41, 105);
            this.rbtMedioPago.Name = "rbtMedioPago";
            this.rbtMedioPago.Size = new System.Drawing.Size(97, 17);
            this.rbtMedioPago.TabIndex = 22;
            this.rbtMedioPago.TabStop = true;
            this.rbtMedioPago.Text = "Medio de Pago";
            this.rbtMedioPago.UseVisualStyleBackColor = true;
            this.rbtMedioPago.CheckedChanged += new System.EventHandler(this.rbtMedioPago_CheckedChanged);
            // 
            // rbtUsuario
            // 
            this.rbtUsuario.AutoSize = true;
            this.rbtUsuario.Location = new System.Drawing.Point(41, 128);
            this.rbtUsuario.Name = "rbtUsuario";
            this.rbtUsuario.Size = new System.Drawing.Size(61, 17);
            this.rbtUsuario.TabIndex = 23;
            this.rbtUsuario.TabStop = true;
            this.rbtUsuario.Text = "Usuario";
            this.rbtUsuario.UseVisualStyleBackColor = true;
            this.rbtUsuario.CheckedChanged += new System.EventHandler(this.rbtUsuario_CheckedChanged);
            // 
            // rbtDescripcionProducto
            // 
            this.rbtDescripcionProducto.AutoSize = true;
            this.rbtDescripcionProducto.Location = new System.Drawing.Point(41, 151);
            this.rbtDescripcionProducto.Name = "rbtDescripcionProducto";
            this.rbtDescripcionProducto.Size = new System.Drawing.Size(68, 17);
            this.rbtDescripcionProducto.TabIndex = 24;
            this.rbtDescripcionProducto.TabStop = true;
            this.rbtDescripcionProducto.Text = "Producto";
            this.rbtDescripcionProducto.UseVisualStyleBackColor = true;
            this.rbtDescripcionProducto.CheckedChanged += new System.EventHandler(this.rbtDescripcionProducto_CheckedChanged);
            // 
            // grillaVentas
            // 
            this.grillaVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaVentas.Location = new System.Drawing.Point(229, 40);
            this.grillaVentas.Name = "grillaVentas";
            this.grillaVentas.Size = new System.Drawing.Size(559, 294);
            this.grillaVentas.TabIndex = 25;
            // 
            // btnVer
            // 
            this.btnVer.Location = new System.Drawing.Point(63, 274);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(75, 23);
            this.btnVer.TabIndex = 26;
            this.btnVer.Text = "Ver";
            this.btnVer.UseVisualStyleBackColor = true;
            this.btnVer.Click += new System.EventHandler(this.btnVer_Click);
            // 
            // Venta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnVer);
            this.Controls.Add(this.grillaVentas);
            this.Controls.Add(this.rbtDescripcionProducto);
            this.Controls.Add(this.rbtUsuario);
            this.Controls.Add(this.rbtMedioPago);
            this.Controls.Add(this.rbtIdVenta);
            this.Controls.Add(this.comboTipoFiltro);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnVolver);
            this.Name = "Venta";
            this.Text = "Venta";
            this.Load += new System.EventHandler(this.Venta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grillaVentas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboTipoFiltro;
        private System.Windows.Forms.RadioButton rbtIdVenta;
        private System.Windows.Forms.RadioButton rbtMedioPago;
        private System.Windows.Forms.RadioButton rbtUsuario;
        private System.Windows.Forms.RadioButton rbtDescripcionProducto;
        private System.Windows.Forms.DataGridView grillaVentas;
        private System.Windows.Forms.Button btnVer;
    }
}