﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Presentación.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Pedidos
{
    public partial class Pedidos : Form
    {
        private string rol;

        private int idUsuarioLogueado;
        public Pedidos(string rol, int idUsuarioLogueado)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuarioLogueado = idUsuarioLogueado;
        }

        private int idPedido = 0;
        private PrintDocument printDocument1;


        private void Pedidos_Mounted(object sender, EventArgs e)
        {
           
        }
        private void Pedidos_Load(object sender, EventArgs e)
        {
            btnNuevaVenta.Enabled = false;

            btnConfirmar.Enabled = true;

            // consulta por punto critco de stock

            DataSet stocck = new DataSet();
            Negocio.Stock listaPuntoCritico = new Negocio.Stock();
            stocck = listaPuntoCritico.ObtenerPuntoCritico();


            lblPuntoCritico.Visible = false;    
            lstPuntoCritico.Visible = false;
            lstPuntoCritico.DataSource = stocck.Tables[0];
            lstPuntoCritico.DisplayMember = "descripcion";
            lstPuntoCritico.ValueMember = "Id_Stock";

            if (lstPuntoCritico.Items.Count > 0)
            {
                lblPuntoCritico.Visible = true;
                lstPuntoCritico.Visible = true;
                MessageBox.Show("Tiene productos con Stock critico, consulte");
            }


            grillaProductosPedidos.Columns.Add("sabor", "Helado");
            grillaProductosPedidos.Columns.Add("precioArticulo", "Precio");
            grillaProductosPedidos.Columns.Add("cantidad", "Cantidad");

            comboIng3.Enabled = false;
            comboIng4.Enabled = false;
            comboIng5.Enabled = false;

            txtCantida3.Enabled = false;
            txtCantida4.Enabled = false;
            txtCantida5.Enabled = false;

            txtCantida1.Text = "1";
            txtCantida2.Text = "1";
            txtCantida3.Text = "0";
            txtCantida4.Text = "0";
            txtCantida5.Text = "0";

            comboIng3.SelectedValue = 0;
            comboIng4.SelectedValue = 0;
            comboIng5.SelectedValue = 0;

            DataSet lsStock1 = new DataSet();
            Negocio.Stock listaDescripcion1 = new Negocio.Stock();
            lsStock1 = listaDescripcion1.ObtenerStock();

            DataSet lsStock2 = new DataSet();
            Negocio.Stock listaDescripcion2 = new Negocio.Stock();
            lsStock2 = listaDescripcion2.ObtenerStock();

            DataSet lsStock3 = new DataSet();
            Negocio.Stock listaDescripcion3 = new Negocio.Stock();
            lsStock3 = listaDescripcion3.ObtenerStock();

            DataSet lsStock4 = new DataSet();
            Negocio.Stock listaDescripcion4 = new Negocio.Stock();
            lsStock4 = listaDescripcion4.ObtenerStock();

            DataSet lsStock5 = new DataSet();
            Negocio.Stock listaDescripcion5 = new Negocio.Stock();
            lsStock5 = listaDescripcion5.ObtenerStock();

            comboIng1.DataSource = lsStock1.Tables[0];
            comboIng1.DisplayMember = "descripcion";
            comboIng1.ValueMember = "Cod Stock";

            comboIng2.DataSource = lsStock2.Tables[0];
            comboIng2.DisplayMember = "descripcion";
            comboIng2.ValueMember = "Cod Stock";

            comboIng3.DataSource = lsStock3.Tables[0];
            comboIng3.DisplayMember = "descripcion";
            comboIng3.ValueMember = "Cod Stock";

            comboIng4.DataSource = lsStock4.Tables[0];
            comboIng4.DisplayMember = "descripcion";
            comboIng4.ValueMember = "Cod Stock";

            comboIng5.DataSource = lsStock5.Tables[0];
            comboIng5.DisplayMember = "descripcion";
            comboIng5.ValueMember = "Cod Stock";
        }

        private void txtDNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
        private void cheIng3_CheckedChanged(object sender, EventArgs e)
        {
            if (cheIng3.Checked)
            {
                comboIng3.Enabled = true;
                txtCantida3.Enabled = true;
            } else
            {
                comboIng3.Enabled = false;
                txtCantida3.Enabled = false;
                txtCantida3.Text = "0";
                comboIng3.SelectedValue = 0;
            }
        }

        private void cheIng4_CheckedChanged(object sender, EventArgs e)
        {
            if (cheIng4.Checked)
            {
                comboIng4.Enabled = true;
                txtCantida4.Enabled = true;
            }
            else
            {
                comboIng4.Enabled = false;
                txtCantida4.Enabled = false;
                txtCantida4.Text = "0";
                comboIng4.SelectedValue = 0;
            }
        }

        private void cheIng5_CheckedChanged(object sender, EventArgs e)
        {
            if (cheIng5.Checked)
            {
                comboIng5.Enabled = true;
                txtCantida5.Enabled = true;
            }
            else
            {
                comboIng5.Enabled = false;
                txtCantida5.Enabled = false;
                txtCantida5.Text = "0";
                comboIng5.SelectedValue = 0;
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MenuGerenal menuGeneral = new MenuGerenal(rol, idUsuarioLogueado);
            this.Hide();
            menuGeneral.Show();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Negocio.Stock stock = new Negocio.Stock();
            string precio = "";
            string precio2 = "";
            string suma = "";

            if (comboHabilitado(comboIng1))
            {
                precio = (Convert.ToDecimal(stock.ObtenerPrecioStock(Convert.ToInt32(comboIng1.SelectedValue))) * Convert.ToDecimal(txtCantida1.Text)).ToString();
                precio2 = (Convert.ToDecimal(stock.ObtenerPrecioStock(Convert.ToInt32(comboIng2.SelectedValue))) * Convert.ToDecimal(txtCantida2.Text)).ToString();
               // precio = stock.ObtenerPrecioStock(Convert.ToInt32(comboIng1.SelectedValue));
                suma = (Convert.ToDecimal(precio) + Convert.ToDecimal(precio2)).ToString();
                grillaProductosPedidos.Rows.Add(comboIng1.Text, "0", txtCantida1.Text);
            }
            if (comboHabilitado(comboIng2))
            {
                precio = (Convert.ToDecimal(stock.ObtenerPrecioStock(Convert.ToInt32(comboIng2.SelectedValue))) * Convert.ToDecimal(txtCantida2.Text)).ToString();
                grillaProductosPedidos.Rows.Add(comboIng2.Text, precio, txtCantida2.Text);
            }
            if (comboHabilitado(comboIng3))
            {
                precio = (Convert.ToDecimal(stock.ObtenerPrecioStock(Convert.ToInt32(comboIng3.SelectedValue))) * Convert.ToDecimal(txtCantida3.Text)).ToString();
                grillaProductosPedidos.Rows.Add(comboIng3.Text, precio, txtCantida3.Text);
                suma = (Convert.ToDecimal(suma) + Convert.ToDecimal(precio)).ToString();
            }
            if (comboHabilitado(comboIng4))
            {
                precio = (Convert.ToDecimal(stock.ObtenerPrecioStock(Convert.ToInt32(comboIng4.SelectedValue))) * Convert.ToDecimal(txtCantida4.Text)).ToString();
                grillaProductosPedidos.Rows.Add(comboIng4.Text, precio, txtCantida4.Text);
                suma = (Convert.ToDecimal(suma) + Convert.ToDecimal(precio)).ToString();
            }
            if (comboHabilitado(comboIng5))
            {
                precio = (Convert.ToDecimal(stock.ObtenerPrecioStock(Convert.ToInt32(comboIng5.SelectedValue))) * Convert.ToDecimal(txtCantida5.Text)).ToString();
                grillaProductosPedidos.Rows.Add(comboIng5.Text, precio, txtCantida5.Text);
                suma = (Convert.ToDecimal(suma) + Convert.ToDecimal(precio)).ToString();
            }
            if (comboHabilitado(comboIng1))
            {
                grillaProductosPedidos.Rows.Add("0", "0", "0");
            }

        }

        private Boolean comboHabilitado(ComboBox comboIng)
        {
            Boolean habilitado = false;

            if(comboIng.Enabled) habilitado = true;

            return habilitado;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (grillaProductosPedidos.Rows.Count < 2) 
            {
                MessageBox.Show("Ingrese productos a vender");
                    return;
            } 
            
            DialogResult dialogResult = MessageBox.Show("¿Confirmar Venta?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            string medioPago = "";

            if (rbtEfectivo.Checked)
            {
                medioPago = "Efectivo";

            } else if (rbtTarjeta.Checked)
            {
                medioPago = "Tarjeta";
            }

            int idVenta = 0;

            Negocio.Venta venta = new Negocio.Venta();
            idVenta = venta.ObtenerIdUltimaVenta();

            idVenta++;

            // consultar dni de cliente para otorgar Descuento
            Entidades.Cliente cliente = new Entidades.Cliente();
            cliente.Dni = txtDni.Text;
            string dni = consultaDniCliente(cliente);
            decimal descuento = 1;
            if (dni != null && dni != "")
            {
                descuento = 0;
            }
            try
            {
                decimal total = 0;
                decimal [] montoTotal = new decimal [grillaProductosPedidos.Rows.Count];
            if (grillaProductosPedidos.Rows.Count > 0)
            {
                for (int fila = 0; fila < grillaProductosPedidos.Rows.Count - 1; fila++)
                {
                    for (int col = 0; col < grillaProductosPedidos.Rows[fila].Cells.Count; col++)
                    {
                        string valor = "";
                        if (grillaProductosPedidos.Rows[fila].Cells[col].Value != null)
                        {
                            valor = grillaProductosPedidos.Rows[fila].Cells[col].Value.ToString();
                        } else
                        {
                            valor = "";
                        }
                        if (valor != "" && col != 2)
                        {
                           // actualizarStock(grillaProductosPedidos.Rows[fila].Cells[col].Value.ToString());
                        }
                    }
                    string valor2 = grillaProductosPedidos.Rows[fila].Cells[1].Value.ToString();

                    if (valor2 != "")
                    {
                        total = total + Convert.ToDecimal(grillaProductosPedidos.Rows[fila].Cells[1].Value);
                        montoTotal[fila] = Convert.ToDecimal(grillaProductosPedidos.Rows[fila].Cells[1].Value);
                        actualizarStock(grillaProductosPedidos.Rows[fila].Cells[0].Value.ToString(), Convert.ToDecimal(grillaProductosPedidos.Rows[fila].Cells[2].Value));
                        registrarVenta(grillaProductosPedidos.Rows[fila].Cells[0].Value.ToString(), Convert.ToDecimal(grillaProductosPedidos.Rows[fila].Cells[2].Value), montoTotal[fila], descuento, idVenta, medioPago);
                    } 
                }
                if (descuento < 1)
                    {
                        grillaProductosPedidos.Rows.Add("Bonificacion", "10%","0");
                        total = total - (total * 10 / 100);
                    }
                grillaProductosPedidos.Rows.Add("Total", "0", total);
            }
                MessageBox.Show("Venta Realizada con exito");
                btnNuevaVenta.Enabled = true;
                btnConfirmar.Enabled = false;
                imprimirPDF();
            }
            catch (Exception)
            {
                MessageBox.Show("Ha ocurrido un error");
            }
        }

        private void actualizarStock(string descripcionProducto, decimal cantidad)
        {
            //metodo para obtener el id de articulo por descripcion
            Negocio.Stock stock = new Negocio.Stock();
            int id_stock = stock.ObtenerIdPorductoPorDescripcion(descripcionProducto);
            
            //crear entidad producto pedido para actualizar el stock por ventas
            Entidades.ProductoPedido stockEditar = new Entidades.ProductoPedido();
            stockEditar.Ing1 = id_stock;
            stockEditar.Cant1 = cantidad;

            Negocio.ProductoPedido stockEditarVentas = new Negocio.ProductoPedido();
            int respuesta = stockEditarVentas.EditarStock(stockEditar);
        }

        private void registrarVenta(string descripcionProducto, decimal cantidad, decimal total, decimal descuento, int idVenta, string medioPago)
        {
            //metodo para obtener el id de articulo por descripcion
            Negocio.Stock stock = new Negocio.Stock();
            int id_stock = stock.ObtenerIdPorductoPorDescripcion(descripcionProducto);

            //crear entidad producto pedido para actualizar el stock por ventas

            //verificar descuento si hay aplicar 10% a total

            if (descuento < 1)
            {
                total = total - (total * 10 / 100);
            }

            Entidades.Venta venta = new Entidades.Venta();
            venta.IdProdcuto = id_stock;
            venta.Cantidad = cantidad;
            venta.Fecha = DateTime.Now;
            venta.IdUsuario = idUsuarioLogueado;
            venta.IdVenta = idVenta;
            venta.MedioPago = medioPago;
            venta.MontoTotal = total;

            Negocio.Venta insertVenta = new Negocio.Venta();
            string respuesta = insertVenta.InsertVenta(venta);
        }

        public string consultaDniCliente(Entidades.Cliente cliente)
        {
            Negocio.Cliente negCliente = new Negocio.Cliente();
           return negCliente.ObtenerClientePorDni(cliente);

        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            grillaProductosPedidos.Columns.Clear();
            grillaProductosPedidos.Rows.Clear();
            Pedidos_Load(sender, e);
        }

        private void imprimirPDF()
        {
            DialogResult dialogResult = MessageBox.Show("¿Descargar Ticket?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            if (grillaProductosPedidos.RowCount == 0)
            {
                MessageBox.Show("No Hay Datos Para Realizar Un Reporte");
            }
            else
            {    //ESCOJE A RUTA DONDE GUARDAREMOS EL PDF
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    string filename = save.FileName;
                    Document doc = new Document(PageSize.A3, 9, 9, 9, 9);
                    Chunk encab = new Chunk("Ticket", FontFactory.GetFont("COURIER", 18));
                    try
                    {
                        FileStream file = new FileStream(filename, FileMode.OpenOrCreate);
                        PdfWriter writer = PdfWriter.GetInstance(doc, file);
                        writer.ViewerPreferences = PdfWriter.PageModeUseThumbs;
                        writer.ViewerPreferences = PdfWriter.PageLayoutOneColumn;
                        doc.Open();

                        doc.Add(new Paragraph(encab));
                        GenerarDocumentos(doc);

                        Process.Start(filename);
                        doc.Close();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        public void GenerarDocumentos(Document document)
        {
            //se crea un objeto PdfTable con el numero de columnas del dataGridView 
            PdfPTable datatable = new PdfPTable(grillaProductosPedidos.ColumnCount);

            //asignamos algunas propiedades para el diseño del pdf 
            datatable.DefaultCell.Padding = 1;
            float[] headerwidths = GetTamañoColumnas(grillaProductosPedidos);

            datatable.SetWidths(headerwidths);
            datatable.WidthPercentage = 100;
            datatable.DefaultCell.BorderWidth = 1;

            //DEFINIMOS EL COLOR DE LAS CELDAS EN EL PDF
            datatable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.WHITE;

            //DEFINIMOS EL COLOR DE LOS BORDES
            datatable.DefaultCell.BorderColor = iTextSharp.text.BaseColor.BLACK;

            //LA FUENTE DE NUESTRO TEXTO
            iTextSharp.text.Font fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA);

            Phrase objP = new Phrase("A", fuente);

            datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            //SE GENERA EL ENCABEZADO DE LA TABLA EN EL PDF 
            for (int i = 0; i < grillaProductosPedidos.ColumnCount; i++)
            {

                objP = new Phrase(grillaProductosPedidos.Columns[i].HeaderText, fuente);
                datatable.HorizontalAlignment = Element.ALIGN_CENTER;

                datatable.AddCell(objP);

            }
            datatable.HeaderRows = 2;

            datatable.DefaultCell.BorderWidth = 1;

            //SE GENERA EL CUERPO DEL PDF
            for (int i = 0; i < grillaProductosPedidos.RowCount - 1; i++)
            {
                for (int j = 0; j < grillaProductosPedidos.ColumnCount; j++)
                {
                    objP = new Phrase(grillaProductosPedidos[j, i].Value.ToString(), fuente);
                    datatable.AddCell(objP);
                }
                datatable.CompleteRow();
            }
            document.Add(datatable);
        }

        //Función que obtiene los tamaños de las columnas del datagridview
        public float[] GetTamañoColumnas(DataGridView dg)
        {
            //Tomamos el numero de columnas
            float[] values = new float[dg.ColumnCount];
            for (int i = 0; i < dg.ColumnCount; i++)
            {
                //Tomamos el ancho de cada columna
                values[i] = (float)dg.Columns[i].Width;
            }
            return values;
        }

        private void txtCantida1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboIng1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
