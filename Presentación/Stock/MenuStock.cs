﻿using Presentación.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Stock
{
    public partial class MenuStock : Form
    {
        private string rol;

        private int idUsuarioLogueado;
        public MenuStock(string rol, int idUsuarioLogueado)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuarioLogueado = idUsuarioLogueado;
        }

        private int idStock = 0;

        private void MenuStock_Load(object sender, EventArgs e)
        {
            rbtAlta.Checked = true;
            DataSet lsStock = new DataSet();
            Negocio.Stock listaProductos = new Negocio.Stock();
            lsStock = listaProductos.ObtenerStock();

            // llenamos grilla
            grillaProductos.DataSource = lsStock;
            grillaProductos.DataMember = "productos";

            DataSet lsProveedor = new DataSet();
            Negocio.Proveedor listaProveedores = new Negocio.Proveedor();
            lsProveedor = listaProveedores.ObtenerProveedores();

            comboProveedor.DataSource = lsProveedor.Tables[0];
            comboProveedor.DisplayMember = "RazonSocial";
            comboProveedor.ValueMember = "Id_Proveedor";
        }

        private void txtDNI_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            MenuGerenal menuGeneral = new MenuGerenal(rol, idUsuarioLogueado);
            this.Hide();
            menuGeneral.Show();
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {

            if (txtCantidad.Text == "")
            {
                MessageBox.Show("Ingrese una Cantidad");
                return;
            }

            if (txtCantidadMinima.Text == "")
            {
                MessageBox.Show("Ingrese una Cantida Minima");
                return;
            }

            if (txtDescripcion.Text == "")
            {
                MessageBox.Show("Ingrese una Descripcion");
                return;
            }

            if (txtPrecio.Text == "")
            {
                MessageBox.Show("Ingrese un precio");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea agregar este producto?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            {
                Entidades.Stock objEntidadStock = new Entidades.Stock();
                objEntidadStock.Descripcion = txtDescripcion.Text;
                objEntidadStock.Cantidad = Convert.ToDecimal(txtCantidad.Text);
                objEntidadStock.CantidadMinima = Convert.ToInt32(txtCantidadMinima.Text);
                objEntidadStock.IdProveedor = Convert.ToInt32(comboProveedor.SelectedValue);
                objEntidadStock.IdUsuario = idUsuarioLogueado;
                objEntidadStock.Precio = Convert.ToDecimal(txtPrecio.Text);
                Negocio.Stock objNegocioStock = new Negocio.Stock();
                string respuesta = objNegocioStock.InsertStock(objEntidadStock);
                if (respuesta == "OK")
                {
                    MessageBox.Show("Producto ingresado con exito");
                    MenuStock_Load(sender, e);
                }
                else
                {
                    MessageBox.Show(respuesta);
                }
            }
        }

        private void grillaProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtId.Text = grillaProductos.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtDescripcion.Text = grillaProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtCantidad.Text = grillaProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtCantidadMinima.Text = grillaProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtPrecio.Text = grillaProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
            }
            catch (Exception err)
            {
                MessageBox.Show("error en: " + err);
            }
        }

        private void btnModificacion_Click(object sender, EventArgs e)
        {

            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese el ID del Stock a modificar");
                return;
            }

            if (txtCantidad.Text == "")
            {
                MessageBox.Show("Ingrese una Cantidad");
                return;
            }

            if (txtCantidadMinima.Text == "")
            {
                MessageBox.Show("Ingrese una Cantida Minima");
                return;
            }

            if (txtPrecio.Text == "")
            {
                MessageBox.Show("Ingrese un precio");
                return;
            }

            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese el Id del Prodcuto a borrar");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea modificar este stock?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }
            


            Entidades.Stock stockEditar = new Entidades.Stock();
            stockEditar.IdStock = Convert.ToInt32(txtId.Text);
            stockEditar.Cantidad = Convert.ToDecimal(txtCantidad.Text);
            stockEditar.CantidadMinima = Convert.ToInt32(txtCantidadMinima.Text);
            stockEditar.Precio = Convert.ToDecimal(txtPrecio.Text);
            stockEditar.IdUsuario = idUsuarioLogueado;
            stockEditar.IdProveedor = Convert.ToInt32(comboProveedor.SelectedValue);


            Negocio.Stock stock = new Negocio.Stock();
            int respuesta = stock.EditarStock(stockEditar);

            if (respuesta > 0)
            {
                MessageBox.Show("Producto editado");
                MenuStock_Load(sender, e);
            }
            else
            {
                MessageBox.Show("error al intentar borrar");
            }
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {

            if (txtId.Text == "")
            {
                MessageBox.Show("Ingrese el ID del Stock a modificar");
                return;
            }

            DialogResult dialogResult = MessageBox.Show("¿Desea eliminar este producto?", "Heladeria Esperanza", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            Entidades.Stock IdStock = new Entidades.Stock();
            IdStock.IdStock = Convert.ToInt32(txtId.Text);

            Negocio.Stock stock = new Negocio.Stock();
            int respuesta = stock.EliminarStock(IdStock);

            if (respuesta > 0)
            {
                MessageBox.Show("Producto eliminado");
                MenuStock_Load(sender, e);
            }
            else
            {
                MessageBox.Show("error al intentar borrar");
            }
        }

        private void rbtAlta_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtAlta.Checked)
            {
                txtDescripcion.Enabled = true;
                txtCantidad.Enabled = true;
                txtCantidadMinima.Enabled = true;
                comboProveedor.Enabled = true;
                txtPrecio.Enabled = true;
                txtId.Enabled = false;
                btnAlta.Enabled = true;
                btnModificacion.Enabled = false;
                btnBaja.Enabled = false;
            }
        }

        private void rbtModificar_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtModificar.Checked)
            {
                txtDescripcion.Enabled = false;
                txtCantidad.Enabled = true;
                txtCantidadMinima.Enabled = true;
                comboProveedor.Enabled = true;
                txtPrecio.Enabled = true;
                txtId.Enabled = true;
                btnAlta.Enabled = false;
                btnModificacion.Enabled = true;
                btnBaja.Enabled = false;
            }
        }

        private void rbtBaja_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtBaja.Checked)
            {
                txtDescripcion.Enabled = false;
                txtCantidad.Enabled = false;
                txtCantidadMinima.Enabled = false;
                comboProveedor.Enabled = false;
                txtPrecio.Enabled = false;
                txtId.Enabled = true;
                btnAlta.Enabled = false;
                btnModificacion.Enabled = false;
                btnBaja.Enabled = true;
            }
        }
    }
}
