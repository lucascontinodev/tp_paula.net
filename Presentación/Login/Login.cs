﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;

namespace Presentación.Login
{
	public partial class Login : Form
	{
		public Login()
		{
			InitializeComponent();
		}

		private void btnEntrar_Click(object sender, EventArgs e)
		{
			Entidades.Login objEntidadLogin = new Entidades.Login();
			objEntidadLogin.Cuenta = txtCuenta.Text;
			objEntidadLogin.Contrasenia = txtContrasenia.Text;
            Negocio.Usuario objNegocioUsuario = new Negocio.Usuario();
            int idUsuario = objNegocioUsuario.LogearUsuario(objEntidadLogin.Cuenta, objEntidadLogin.Contrasenia);
            if (idUsuario != 0)
            {
                Negocio.Usuario tipoUsuario = new Negocio.Usuario();
                string rol = tipoUsuario.RolAsociado(idUsuario);
                this.Hide();
                irAMenu(rol, idUsuario);
            }
            else
            {
                MessageBox.Show("Datos incorrectos");
            }
		}

        private void Login_Load(object sender, EventArgs e)
        {

        }
        private void irAMenu(string rol, int idUsuario)
        {
            Menu.MenuGerenal menu = new Menu.MenuGerenal(rol, idUsuario);
            menu.Show();
        }

        private void btnNuevoUsuario_Click(object sender, EventArgs e)
        {
            Usuario.GestionUsuario menuUsuario = new Usuario.GestionUsuario();
            this.Hide();
            menuUsuario.Show();
        }
    }
}
