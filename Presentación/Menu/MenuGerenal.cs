﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Menu
{
    public partial class MenuGerenal : Form
    {
        private string rol;

        private int idUsuarioLogueado;
        public MenuGerenal(string rol, int idUsuarioLogueado)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuarioLogueado = idUsuarioLogueado;
        }

        private void MenuGerenal_Load(object sender, EventArgs e)
        {
            if (rol == "Encargado" || rol == "Gerente")
            {
                btnStock.Enabled = true;
                btnProveedores.Enabled = true;
                btnVentas.Enabled = true;
                btnPedidos.Enabled = true;
                btnClientes.Enabled = true;
                btnUsuarios.Enabled = true;
            }
            else if (rol == "Cajero")
            {
                btnStock.Enabled = false;
                btnProveedores.Enabled = false;
                btnVentas.Enabled = true;
                btnPedidos.Enabled = true;
                btnClientes.Enabled = true;
                btnUsuarios.Enabled = false;
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            UsuariosRegistrados.UsuariosRegistrados menuUsuarios = new UsuariosRegistrados.UsuariosRegistrados(rol, idUsuarioLogueado);
            this.Hide();
            menuUsuarios.Show();
        }

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            MenuProveedores menuProveedor = new MenuProveedores(rol, idUsuarioLogueado);
            this.Hide();
            menuProveedor.Show();

        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            Cliente.InsertCliente menuCliente = new Cliente.InsertCliente(rol, idUsuarioLogueado);
            this.Hide();
            menuCliente.Show();
        }

        private void btnStock_Click(object sender, EventArgs e)
        {
            Stock.MenuStock menuStock = new Stock.MenuStock(rol, idUsuarioLogueado);
            this.Hide();
            menuStock.Show();
        }

        private void btnPedidos_Click(object sender, EventArgs e)
        {
            Pedidos.Pedidos menuPedidos = new Pedidos.Pedidos(rol, idUsuarioLogueado);
            this.Hide();
            menuPedidos.Show();
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            Pedidos.Venta menuVenta= new Pedidos.Venta(rol, idUsuarioLogueado);
            this.Hide();
            menuVenta.Show();
        }
    }
}
