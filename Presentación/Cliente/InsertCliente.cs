﻿using Presentación.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación.Cliente
{
	public partial class InsertCliente : Form
	{
		private string rol;

		private int idUsuarioLogueado;
		public InsertCliente(string rol, int idUsuarioLogueado)
		{
			InitializeComponent();
			this.rol = rol;
			this.idUsuarioLogueado = idUsuarioLogueado;
		}

		private int idCliente = 0;

		private void btnAñadir_Click(object sender, EventArgs e)
		{
		}


		public void LimpiarCampos()
		{
			txtApellido.Text = "";
			txtDni.Text = "";
			txtDomicilio.Text = "";
			txtTelefono.Text = "";
			txtNombre.Text = "";
			txtId.Text = "";

		}

		private void btnLimpiar_Click(object sender, EventArgs e)
		{
			LimpiarCampos();
		}

        private void InsertCliente_Load(object sender, EventArgs e)
        {
			rbtAlta.Checked = true;
			
			DataSet datosClientes = new DataSet();
			Negocio.Cliente listaCliente = new Negocio.Cliente();
			datosClientes = listaCliente.ObtenerClientes();

			// llenamos grilla
			grillaClientes.DataSource = datosClientes;
			grillaClientes.DataMember = "clientes";
		}

		private void txtDNI_KeyPress(object sender, KeyPressEventArgs e)
		{
			//Para obligar a que sólo se introduzcan números
			if (Char.IsDigit(e.KeyChar))
			{
				e.Handled = false;
			}
			else
			  if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
			{
				e.Handled = false;
			}
			else
			{
				//el resto de teclas pulsadas se desactivan
				e.Handled = true;
			}
		}

		private void btnAlta_Click(object sender, EventArgs e)
        {
			if (txtDni.Text == "")
			{
				MessageBox.Show("Ingrese un DNI");
				return;
			}

			if (txtNombre.Text == "")
			{
				MessageBox.Show("Ingrese un Nombre");
				return;
			}

			if (txtApellido.Text == "")
			{
				MessageBox.Show("Ingrese un Apellido");
				return;
			}

			if (txtTelefono.Text == "")
			{
				MessageBox.Show("Ingrese un Telefono");
				return;
			}

			if (txtDomicilio.Text == "")
			{
				MessageBox.Show("Ingrese un Domicilio");
				return;
			}

			{
				Entidades.Cliente objEntidadCliente = new Entidades.Cliente();
				objEntidadCliente.Nombre = txtNombre.Text;
				objEntidadCliente.Apellido = txtApellido.Text;
				objEntidadCliente.Dni = txtDni.Text;
				objEntidadCliente.Telefono = txtTelefono.Text;
				objEntidadCliente.Domicilio = txtDomicilio.Text;
				Negocio.Cliente objNegocioCliente = new Negocio.Cliente();
				string respuesta = objNegocioCliente.InsertCliente(objEntidadCliente);
				if (respuesta == "OK")
				{
					MessageBox.Show("Cliente ingresado con exito");
					InsertCliente_Load(sender, e);
				}
				else
				{
					MessageBox.Show(respuesta);
				}
			}
		}

        private void btnVolver_Click(object sender, EventArgs e)
        {
			MenuGerenal menuGeneral = new MenuGerenal(rol, idUsuarioLogueado);
			this.Hide();
			menuGeneral.Show();
		}

		private void grillaClientes_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			try
			{
				txtId.Text = grillaClientes.Rows[e.RowIndex].Cells[0].Value.ToString();
				txtNombre.Text = grillaClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
				txtApellido.Text = grillaClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
				txtDni.Text = grillaClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
				txtDomicilio.Text = grillaClientes.Rows[e.RowIndex].Cells[4].Value.ToString();
				txtTelefono.Text = grillaClientes.Rows[e.RowIndex].Cells[5].Value.ToString();
			}
			catch (Exception)
			{

			}
		}

        private void btnModificacion_Click(object sender, EventArgs e)
        {
			if (txtId.Text == "")
			{
				MessageBox.Show("Ingrese un ID de cliente");
				return;
			}

			if (txtTelefono.Text == "")
			{
				MessageBox.Show("Ingrese un Telefono");
				return;
			}

			if (txtDomicilio.Text == "")
			{
				MessageBox.Show("Ingrese un Domicilio");
				return;
			}

			Entidades.Cliente clienteEditar = new Entidades.Cliente();
			clienteEditar.IdCliente = Convert.ToInt32(txtId.Text);
			clienteEditar.Domicilio = txtDomicilio.Text;
			clienteEditar.Telefono = txtTelefono.Text;


			Negocio.Cliente cliente = new Negocio.Cliente();
			int respuesta = cliente.EditarCliente(clienteEditar);

			if (respuesta > 0)
			{
				MessageBox.Show("Cliente editado");
				InsertCliente_Load(sender, e);
			}
			else
			{
				MessageBox.Show("error al intentar borrar");
			}
		}

        private void rbtAlta_CheckedChanged(object sender, EventArgs e)
        {
			if (rbtAlta.Checked)
			{
				txtApellido.Enabled = true;
				txtNombre.Enabled = true;
				txtTelefono.Enabled = true;
				txtDni.Enabled = true;
				txtDomicilio.Enabled = true;
				txtId.Enabled = false;
				btnAlta.Enabled = true;
				btnModificacion.Enabled = false;
			}
		}

        private void rbtModificar_CheckedChanged(object sender, EventArgs e)
        {
			if (rbtModificar.Checked)
			{
				txtApellido.Enabled = false;
				txtNombre.Enabled = false;
				txtTelefono.Enabled = true;
				txtDni.Enabled = false;
				txtDomicilio.Enabled = true;
				txtId.Enabled = true;
				btnAlta.Enabled = false;
				btnModificacion.Enabled = true;
			}
		}
    }
}
