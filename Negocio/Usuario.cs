﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
	public class Usuario
	{
		public string InsertUsuario(Entidades.Login usuarioAInsertar, int rolObtenido)
		{
			Datos.Usuario objDatos = new Datos.Usuario();
			return objDatos.InsertUsuario(usuarioAInsertar, rolObtenido);
		}

        public int LogearUsuario(string cuenta, string contrasenia)
        {
            Datos.Usuario objDatos = new Datos.Usuario();
            return objDatos.LogearUsuario(cuenta, contrasenia);
        }

        public string RolAsociado(int id_usuario)
        {
            Datos.Usuario objDatos = new Datos.Usuario();
            return objDatos.RolAsociado(id_usuario);
        }

        public int ObtenerRol(string rolaObtener)
        {
            Datos.Usuario objDatos = new Datos.Usuario();
            return objDatos.ObtenerRol(rolaObtener);
        }

        public DataSet ObtenerUsuarios()
        {
            Datos.Usuario objDatos = new Datos.Usuario();
            return objDatos.ObtenerUsuarios();
        }

        public int EditarUsuario(Entidades.Usuario usuarioAEditar)
        {
            Datos.Usuario objDatos = new Datos.Usuario();
            return objDatos.EditarUsuario(usuarioAEditar);
        }

        public int EliminarUsuario(Entidades.Usuario usuarioAEliminar)
        {
            Datos.Usuario objDatos = new Datos.Usuario();
            return objDatos.EliminarUsuario(usuarioAEliminar);
        }


    }
}
