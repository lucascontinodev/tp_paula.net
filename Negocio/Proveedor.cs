﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Proveedor
    {
        public string InsertProveedor(Entidades.Proveedor proveedorAinsertar)
        {
            Datos.Proveedor objDatos = new Datos.Proveedor();
            return objDatos.InsertProveedor(proveedorAinsertar);
        }

        public DataSet ObtenerProveedores()
        {
            Datos.Proveedor objDatos = new Datos.Proveedor();
            return objDatos.ObtenerProveedores();
        }

        public int EliminarProveedor(Entidades.Proveedor proveedorAEliminar)
        {
            Datos.Proveedor objDatos = new Datos.Proveedor();
            return objDatos.EliminarProveedor(proveedorAEliminar);
        }

        public int EditarProveedor(Entidades.Proveedor proveedorAEditar)
        {
            Datos.Proveedor objDatos = new Datos.Proveedor();
            return objDatos.EditarProveedor(proveedorAEditar);
        }
    }
}
