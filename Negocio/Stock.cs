﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Stock
    {
        public DataSet ObtenerStock()
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.ObtenerStock();
        }

        public DataSet ObtenerPuntoCritico()
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.ObtenerPuntoCritico();
        }

        public string ObtenerPrecioStock(int id_stock)
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.ObtenerPrecioStock(id_stock);
        }

        public int ObtenerIdPorductoPorDescripcion(string descripcionProducto)
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.ObtenerIdPorductoPorDescripcion(descripcionProducto);
        }

        public string InsertStock(Entidades.Stock stockAinsertar)
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.InsertStock(stockAinsertar);
        }

        public int EditarStock(Entidades.Stock stockoAEditar)
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.EditarStock(stockoAEditar);
        }

        public int EliminarStock(Entidades.Stock stockAEliminar)
        {
            Datos.Stock objDatos = new Datos.Stock();
            return objDatos.EliminarStock(stockAEliminar);
        }
    }
}
