﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class Venta
    {
        public int ObtenerIdUltimaVenta()
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerIdUltimaVenta();
        }

        public string InsertVenta(Entidades.Venta ventaAinsertar)
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.InsertVenta(ventaAinsertar);
        }

        public DataSet ObtenerVentas()
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerVentas();
        }

        public DataSet ObtenerMedioPago()
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerMedioPago();
        }

        public DataSet ObtenerUsuario()
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerUsuario();
        }

        public DataSet ObtenerProducto()
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerProducto();
        }

        public DataSet ObtenerListaPorId(int idVenta)
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerListaPorId(idVenta);
        }

        public DataSet ObtenerListaPorMedioPago(string MedioPago)
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerListaPorMedioPago(MedioPago);
        }

        public DataSet ObtenerListaPorUsuario(string Usuario)
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerListaPorUsuario(Usuario);
        }

        public DataSet ObtenerListaPorProducto(string Producto)
        {
            Datos.Venta objDatos = new Datos.Venta();
            return objDatos.ObtenerListaPorProducto(Producto);
        }
    }
}
